#include <memory>
#include "priority_queue.h"

int main(int argc, char** argv) {
  std::vector<int> vec = {10, 7, 15, 21, 8, 5, 3, 4, 2, 1};
  PriorityQueue<int> p_que(vec);

  std::cout << "Heapified: ";
  p_que.print_array();
  std::cout << "Front: " << p_que.min() << std::endl;
  p_que.pop_min();
  std::cout << "Popped: ";
  p_que.print_array();

  // p_que.insert(0);
  // std::cout << "New element inserted: ";
  // p_que.print_array();

  std::vector<int> vec_sorted = p_que.heap_sort();
  std::cout << "sorted: " << std::endl;
  for (int i : vec_sorted) std::cout << i << " ";
  return 0;
}
