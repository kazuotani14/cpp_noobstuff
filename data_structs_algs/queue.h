#include <exception>
#include <vector>
#include "linked_list.h"

namespace kazulib {

template <typename T>
class Queue_LinkedList {
 public:
  Queue_LinkedList(){};

  void enqueue(T value) { list_.push_back(value); }

  T dequeue() {
    if (!empty()) {
      return list_.pop_back();
    } else {
      throw std::runtime_error("empty queue");
    }
  }

  bool empty() { return list_.empty(); }

  void print() { list_.print(); }

 private:
  LinkedList<T> list_;
};

template <typename T>
class Queue_FixedSizeArray {
 public:
  Queue_FixedSizeArray(int capacity) : array_(capacity){};

  void enqueue(T value) {
    if (!full()) {
      array_[n_elements_++] = value;
    } else {
      throw std::runtime_error("full queue");
    }
  }

  T dequeue() {
    if (!empty()) {
      return array_[--n_elements_];
    } else {
      throw std::runtime_error("empty queue");
    }
  }

  bool empty() { return (n_elements_ == 0); }

  bool full() { return (n_elements_ == array_.size()); }

  void print() {
    for (int i = 0; i < n_elements_; i++) {
      std::cout << array_[i] << " ";
    }
    std::cout << "\n";
  }

 private:
  std::vector<T> array_;
  int n_elements_ = 0;
};
}  // namespace kazulib
