#ifndef _TREE_H_
#define _TREE_H_

#include <algorithm>
#include <exception>
#include <memory>
#include <stack>
#include <vector>

// TODO implement re-balancing

using std::max;

template <typename T>
struct TreeNode {
  TreeNode(T val) : data(val){};

  std::shared_ptr<TreeNode> left;
  std::shared_ptr<TreeNode> right;
  T data;
};

template <typename T>
class BinarySearchTree {
 public:
  BinarySearchTree(){};
  // BinarySearchTree(std::vector<T> init_vals);

  void insert(T value) {
    std::shared_ptr<TreeNode<T>>& pos = find_position(value);

    if (!pos) {
      pos.reset(new TreeNode<T>(value));
    } else {
      throw std::runtime_error("value already exists");
    }
  }

  bool is_in_tree(T value) {
    std::shared_ptr<TreeNode<T>>& pos = find_position(value);
    return pos ? true : false;
  }

  // print values min to max
  // in order traversal
  void print_values() {
    _print_values(root_);
    std::cout << "\n";
  }

  void print_values_nonrecursive() {
    std::stack<std::shared_ptr<TreeNode<T>>> q;
    std::shared_ptr<TreeNode<T>>& node = root_;

    while (true) {
      if (node) {
        q.push(node);
        node = node->left;
      } else {
        if (!q.empty()) {
          node = q.top();
          q.pop();
          std::cout << node->data << " ";
          node = node->right;
        } else {
          break;
        }
      }
    }
    std::cout << "\n";
  }

  // returns next-highest value in tree after given value
  T get_successor(T value) {
    std::shared_ptr<TreeNode<T>>& pos = _find_position(value, root_);

    if (pos->right) {
      // return min from right side
      std::shared_ptr<TreeNode<T>> node = pos->right;
      while (node->left) {
        node = node->left;
      }
      return node->data;
    } else {
      // search for successor from root
      std::shared_ptr<TreeNode<T>> node = root_;
      std::shared_ptr<TreeNode<T>> succ;
      while (node) {
        if (node->data > value) {
          succ = node;
          node = node->left;
        } else if (node->data < value) {
          node = node->right;
        } else {
          break;
        }
      }
      if (succ) {
        return succ->data;
      } else {
        throw std::runtime_error("successor does not exist");
      }
    }
  }

  void delete_value(T value) {
    std::shared_ptr<TreeNode<T>>& pos = find_position(value);
    if (pos) {
      pos.reset(nullptr);
    } else {
      throw std::runtime_error("value does not exist");
    }
  }

  void delete_tree() {
    if (root_) {
      root_.reset(nullptr);
    }
  }

  int get_node_count() { return _get_node_count(root_); }

  int get_height() { return _get_height(0, root_); }

  T get_min() {
    std::shared_ptr<TreeNode<T>> node = root_;
    while (node->left) {
      node = node->left;
    }
    return node->data;
  }

  T get_max() {
    std::shared_ptr<TreeNode<T>> node = root_;
    while (node->right) {
      node = node->right;
    }
    return node->data;
  }

  std::shared_ptr<TreeNode<T>> root_;  // here just to test
                                       // is_binary_search_tree

 private:
  std::shared_ptr<TreeNode<T>>& find_position(T value) {
    return _find_position(value, root_);
  }

  std::shared_ptr<TreeNode<T>>& _find_position(
      T value, std::shared_ptr<TreeNode<T>>& node) {
    if (!node) {  // if node isn't initialized yet
      return node;
    } else {
      if (node->data > value) {
        return _find_position(value, node->left);
      } else if (node->data < value) {
        return _find_position(value, node->right);
      } else {
        return node;
      }
    }
  }

  int _get_height(int height, std::shared_ptr<TreeNode<T>>& node) {
    if (node) {
      return max(_get_height(height + 1, node->left),
                 _get_height(height + 1, node->right));
    } else {
      return height;
    }
  }

  int _get_node_count(std::shared_ptr<TreeNode<T>>& node) {
    if (node) {
      return 1 + _get_node_count(node->left) + _get_node_count(node->right);
    } else {
      return 0;
    }
  }

  void _print_values(std::shared_ptr<TreeNode<T>>& node) {
    if (node) {
      _print_values(node->left);
      std::cout << node->data << " ";
      _print_values(node->right);
    }
  }
};

template <typename T>
bool is_binary_search_tree(std::shared_ptr<TreeNode<T>> node) {
  if (node) {
    bool right_valid = true;
    bool left_valid = true;
    if (node->right) {
      if (node->data > node->right->data) {
        return false;
      }
      right_valid = is_binary_search_tree(node->right);
    }
    if (node->left) {
      if (node->data < node->left->data) {
        return false;
      }
      left_valid = is_binary_search_tree(node->left);
    }
    return right_valid && left_valid;
  } else {
    return true;
  }
}

#endif
