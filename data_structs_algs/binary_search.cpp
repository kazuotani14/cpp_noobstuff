#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <vector>

// See wikipedia for implementation details
// https://en.wikipedia.org/wiki/Binary_search_algorithm#Implementation_issues

int binary_search(const std::vector<int>& vec, const int find_val) {
  int l = 0;
  int r = vec.size() - 1;
  int m;

  while (l <= r) {
    m = l + (r - l) / 2;  // (l+r)/2 may cause overflow
    if (vec[m] == find_val) {
      return m;
    } else if (vec[m] > find_val) {
      r = m - 1;
    } else if (vec[m] < find_val) {
      l = m + 1;
    }
  }
  return -1;
}

int binary_search_recursive(const std::vector<int>& vec, const int find_val,
                            int min, int max) {
  int mid = (min + max) / 2;
  if (vec[mid] == find_val) {
    return mid;
  } else if (vec[mid] > find_val) {
    return binary_search_recursive(vec, find_val, min, mid - 1);
  } else if (vec[mid] < find_val) {
    return binary_search_recursive(vec, find_val, mid + 1, max);
  }
  return -1;
}

int main(int argc, char** argv) {
  std::srand(std::time(0));  // use current time as seed for random generator

  int n = 50;
  int find_val = std::rand() % 100;
  std::vector<int> vec(n);
  for (int i = 0; i < (n - 1); i++) {
    vec[i] = std::rand() % 100;
  }
  vec[n - 1] = find_val;

  std::sort(vec.begin(), vec.end());

  auto print_vec = [](std::vector<int> v) {
    for (const auto& e : v) std::cout << e << " ";
    std::cout << "\n";
  };
  print_vec(vec);

  auto res_find = std::find(std::begin(vec), std::end(vec), find_val);
  int idx_find = res_find - vec.begin();

  std::cout << "index of " << find_val << ": " << binary_search(vec, find_val)
            << std::endl;
  std::cout << "index of " << find_val << ": "
            << binary_search_recursive(vec, find_val, 0, vec.size() - 1)
            << std::endl;
  std::cout << "index of " << find_val << ": " << idx_find << std::endl;

  return 0;
}
