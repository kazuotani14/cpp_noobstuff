/*
  Implementation of self-balancing (height-balanced) binary search tree :
red-black tree

  Algorithm: https://en.wikipedia.org/wiki/Red%E2%80%93black_tree
  Visualization: https://www.cs.usfca.edu/~galles/visualization/RedBlack.html

  * Keep height of tree balanced on all branches to ensure logarithmic time
access, insertions
  * Insertions become slightly more expensive because the tree must be
re-balanced when the heights of branches get too uneven.

  "Balanced" in this context means that the longest path from the root to a leaf
is at most twice the length of the shortest path. This is achieved by making
oeprations to maintain the following rules:
  * Each node is red or black
  * The root is always black
  * All children of leaves are considered to be black
  * If a node is red, its children are black
  * Every path from a given node to any of its leaf nodes contains the same
number of black nodes

  In Red-Black tree, we use two tools to do balancing: recoloring, rotation.
  We try recoloring first, if recoloring doesn’t work, then we go for rotation

TODO

* Implement re-balancing for delete
* Print tree?
* review

*/

#pragma once

#include <algorithm>
#include <exception>
#include <iomanip>
#include <iostream>
#include <map>
#include <memory>
#include <queue>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using std::cout;
using std::endl;
using std::setw;
using std::shared_ptr;
using std::runtime_error;
using std::max;
using std::pair;
using std::queue;
using std::string;
using std::vector;

enum Color { RED, BLACK };
static std::map<Color, std::string> ColorNames{{RED, "RED"}, {BLACK, "BLACK"}};

template <typename T>
struct TreeNode {
  // TreeNode(T val) : data(val) {};
  TreeNode(T val, TreeNode<T>* p) : data(val), parent(p){};

  T data;
  TreeNode<T>* left = nullptr;
  TreeNode<T>* right = nullptr;
  TreeNode<T>* parent = nullptr;
  Color color = RED;
};

template <typename T>
class RedBlackTree {
 public:
  RedBlackTree(){};

  ~RedBlackTree() { _destruct(root_); }

  TreeNode<T>* Insert(T value);
  void Remove(T value);

  // Used for rotating nodes
  TreeNode<T>* Parent(TreeNode<T>* n);
  TreeNode<T>* Sibling(TreeNode<T>* n);
  TreeNode<T>* Uncle(TreeNode<T>* n);
  TreeNode<T>* Grandparent(TreeNode<T>* n);

  void RotateRight(TreeNode<T>* n);
  void RotateLeft(TreeNode<T>* n);

  // return largest value in tree that is less than value
  T lower_bound(T value);
  // returns first value that is not less than (greater than or equal to) value
  T upper_bound(T value);

  bool is_in_tree(T value);
  int get_node_count();
  int get_height();
  T get_min();
  T get_max();

  // print values min to max
  void print();

  TreeNode<T>* find_position(
      T value);  // TODO remove this; this is just for testing

 private:
  TreeNode<T>* root_;
  bool left_;

  void _destruct(TreeNode<T>* node) {
    if (!node) return;
    if (node->left) _destruct(node->left);
    if (node->right) _destruct(node->right);
    delete node;
  }

  TreeNode<T>* find_position(T value, TreeNode<T>*& parent);
  TreeNode<T>* _find_position(T value, TreeNode<T>* node, TreeNode<T>*& parent);

  void _lower_bound(TreeNode<T>* node, T& lb, T value);
  void _upper_bound(TreeNode<T>* node, T& lb, T value);

  int _get_height(int height, TreeNode<T>* node);
  int _get_node_count(TreeNode<T>* node);
  void _print_values(TreeNode<T>* node, int depth);

  void insert_repair_tree(TreeNode<T>* node);
  void insert_case1(TreeNode<T>* node);
  void insert_case2(TreeNode<T>* node);
  void insert_case3(TreeNode<T>* node);
  void insert_case4(TreeNode<T>* node);

  void remove_has_child(TreeNode<T>* node);
};

#include "red_black_tree.cpp"
