#include "queue.h"

int main(int argc, char** argv) {
  kazulib::Queue_LinkedList<int> q_ll;
  kazulib::Queue_FixedSizeArray<int> q_fsa(3);

  q_ll.enqueue(1);
  q_fsa.enqueue(1);

  q_ll.print();
  q_fsa.print();

  q_ll.enqueue(2);
  q_fsa.enqueue(2);
  q_ll.enqueue(3);
  q_fsa.enqueue(3);

  q_ll.print();
  q_fsa.print();

  std::cout << q_ll.dequeue() << std::endl;
  std::cout << q_fsa.dequeue() << std::endl;

  q_ll.print();
  q_fsa.print();

  std::cout << q_ll.dequeue() << std::endl;
  std::cout << q_fsa.dequeue() << std::endl;
  std::cout << q_ll.dequeue() << std::endl;
  std::cout << q_fsa.dequeue() << std::endl;

  return 0;
}
