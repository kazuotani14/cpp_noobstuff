#include <algorithm>  // find
#include <iostream>
#include <limits>  //inf
#include <map>
#include <queue>  // push, front, pop, empty
#include <set>
#include <stack>  // push, top, pop, empty
#include <string>
#include <utility>
#include <vector>

using namespace std;

/*
// Bidirectional graphs with two representations
    * DFS with adjacency list (recursive)
    * DFS with adjacency list (iterative with stack)
    * DFS with adjacency matrix (recursive)
    * DFS with adjacency matrix (iterative with stack)
    * BFS with adjacency list
    * BFS with adjacency matrix

    * single-source shortest path (Dijkstra)
    * minimum spanning tree

    * DFS-based algorithms (see Aduni videos above):
    *  check for cycle (needed for topological sort, since we'll check for cycle
before starting)
    *  topological sort
    *  count connected components in a graph
    *  list strongly connected components
    *  check for bipartite graph
*/

/*
 * Adjacency lists
 * better for storing sparse graphs
 * To see if a node is connected to another, need to iterate through its
 * neighbor list
 * Adjacency matrix
 * Size scales n^2, so not good for big sparse graphs
 * Better for dense graphs that need index access
 * Objects and pointers
 * better for storing additional information inside nodes, without a separate
 * data structure (which is needed for two other representations)
 */

class Graph {
 public:
  virtual void add_edge(int node1, int node2, double weight) = 0;
  virtual void add_edges(vector<pair<int, int>> edges) = 0;
  virtual void remove_edge(int node1, int node2) = 0;
  virtual vector<int> get_neighbors_of(int node) const = 0;
  virtual int get_num_nodes() const = 0;
};

class Graph_AdjList : public Graph {
 public:
  Graph_AdjList(int size) { adjacency_lists_.resize(size); }

  void add_edge(int node1, int node2, double weight = 1) override {
    adjacency_lists_[node1].push_back(node2);
    adjacency_lists_[node2].push_back(node1);
  }

  void add_edges(vector<pair<int, int>> edges) override {
    for (const auto& edge : edges) {
      add_edge(edge.first, edge.second);
    }
  }

  void remove_edge(int node1, int node2) override {
    vector<int>& node1_list = adjacency_lists_[node1];
    node1_list.erase(std::remove(node1_list.begin(), node1_list.end(), node2),
                     node1_list.end());
    vector<int>& node2_list = adjacency_lists_[node2];
    node2_list.erase(std::remove(node2_list.begin(), node2_list.end(), node1),
                     node2_list.end());
  }

  vector<int> get_neighbors_of(int node) const override {
    return adjacency_lists_[node];
  }

  int get_num_nodes() const override { return adjacency_lists_.size(); }

 private:
  vector<vector<int>> adjacency_lists_;
};

// Has support for edges, since it was simpler to add
class Graph_AdjMat : public Graph {
 public:
  Graph_AdjMat(int size) {
    adjacency_matrix_.resize(size);
    for (auto& row : adjacency_matrix_) row.resize(size);
  }

  virtual void add_edge(int node1, int node2, double weight = 1) override {
    adjacency_matrix_[node1][node2] = adjacency_matrix_[node2][node1] = weight;
  }

  void add_edges(vector<pair<int, int>> edges) override {
    for (const auto& edge : edges) {
      add_edge(edge.first, edge.second, 1);
    }
  }

  void add_edges_weighted(vector<pair<pair<int, int>, double>> edges) {
    for (const auto& edge : edges) {
      add_edge(edge.first.first, edge.first.second, edge.second);
    }
  }

  void remove_edge(int node1, int node2) override {
    adjacency_matrix_[node1][node2] = adjacency_matrix_[node2][node1] = 0;
  }

  double edge_weight_between(int node1, int node2) const {
    return adjacency_matrix_[node1][node2];
  }

  vector<int> get_neighbors_of(int node) const override {
    vector<int> neighbors;
    const vector<int>& node_list = adjacency_matrix_[node];
    for (int i = 0; i < node_list.size(); ++i) {
      if (node_list[i] > 0) neighbors.push_back(i);
    }
    return neighbors;
  }

  vector<pair<double, pair<int, int>>> get_all_edges() const {
    vector<pair<double, pair<int, int>>> edges;
    for (int i = 0; i < adjacency_matrix_.size(); ++i) {
      for (int j = i; j < adjacency_matrix_.size(); ++j) {
        if (adjacency_matrix_[i][j] > 0) {
          edges.push_back({adjacency_matrix_[i][j], {i, j}});
        }
      }
    }
    return edges;
  }

  int get_num_nodes() const override { return adjacency_matrix_.size(); }

 protected:
  vector<vector<int>> adjacency_matrix_;
};

class Graph_AdjMat_Directed : public Graph_AdjMat {
 public:
  Graph_AdjMat_Directed(int size) : Graph_AdjMat(size){};
  virtual void add_edge(int from, int to, double weight = 1) override {
    adjacency_matrix_[from][to] = weight;
  }
};

bool node_in_vec(int node, const vector<int> v) {
  return (std::find(v.begin(), v.end(), node) != v.end());
}

void DepthFirstSearchIterative(const Graph& graph, int start) {
  stack<int> to_visit;
  to_visit.push(start);

  int current_node;
  vector<int> visited;
  while (!to_visit.empty()) {
    current_node = to_visit.top();
    to_visit.pop();
    if (node_in_vec(current_node, visited))
      continue;  // TODO find more elegant way
    visited.push_back(current_node);
    std::cout << current_node << " ";

    vector<int> neighbors = graph.get_neighbors_of(current_node);
    for (const auto& n : neighbors) {
      if (!node_in_vec(n, visited)) {
        to_visit.push(n);
      }
    }
  }
  std::cout << "\n";
}

void _DepthFirstSearchRecursive(const Graph& graph, int visit,
                                vector<int>& visited) {
  std::cout << visit << " ";
  visited.push_back(visit);

  vector<int> neighbors = graph.get_neighbors_of(visit);
  for (const auto& n : neighbors) {
    if (!node_in_vec(n, visited)) {
      _DepthFirstSearchRecursive(graph, n, visited);
    }
  }
}

void DepthFirstSearchRecursive(const Graph& graph, int start) {
  vector<int> visited;
  _DepthFirstSearchRecursive(graph, start, visited);
  std::cout << "\n";
}

// Doesn't make sense to do BFS recursively, since tail-recursion adds to the
// stack
void BreadthFirstSearchIterative(const Graph& graph, int start) {
  queue<int> to_visit;
  to_visit.push(start);

  int current_node;
  vector<int> visited;
  while (!to_visit.empty()) {
    current_node = to_visit.front();
    to_visit.pop();
    if (node_in_vec(current_node, visited))
      continue;  // TODO find more elegant way
    visited.push_back(current_node);
    std::cout << current_node << " ";

    vector<int> neighbors = graph.get_neighbors_of(current_node);
    for (const auto& n : neighbors) {
      if (!node_in_vec(n, visited)) {
        to_visit.push(n);
      }
    }
  }
  std::cout << "\n";
}

/*
  Dijkstra's algorithm
  Single-source shortest path on a graph
  Keep track of: cost-to-go, parent for each node, visited, to_visit (priority
  queue)
*/

using Node = pair<double, int>;  // cost-to-go, node ID
class NodePriorityQueue
    : public priority_queue<Node, vector<Node>, greater<Node>> {
 public:
  // priority_queue::c is protected
  vector<Node>::iterator begin() {
    return priority_queue<Node, vector<Node>, greater<Node>>::c.begin();
  };
  vector<Node>::iterator end() {
    return priority_queue<Node, vector<Node>, greater<Node>>::c.end();
  };
};

// Assuming edge cost is 1 between all nodes
// TODO use map for visited
vector<int> Dijkstra(const Graph& graph, int start, int end) {
  NodePriorityQueue to_visit;
  vector<int> visited;
  vector<double> cost(graph.get_num_nodes(),
                      numeric_limits<double>::infinity());
  vector<int> prev(graph.get_num_nodes(), 0);

  cost[start] = 0;
  to_visit.push({0, start});

  // Find path
  int current;
  while (!to_visit.empty()) {
    current = to_visit.top().second;
    to_visit.pop();
    if (node_in_vec(current, visited)) continue;
    if (current == end) break;
    visited.push_back(current);

    vector<int> neighbors = graph.get_neighbors_of(current);
    for (auto& n : neighbors) {
      double cost_to_go = cost[current] + 1;
      if (cost_to_go < cost[n]) {
        cost[n] = cost_to_go;
        prev[n] = current;
      }
      auto it =
          find_if(to_visit.begin(), to_visit.end(),
                  [&n](const pair<int, int>& p) { return (p.second == n); });

      // update priority of n if it's already in to_visit
      if (it != to_visit.end()) {
        it->first = cost[n];
      } else if (!node_in_vec(n, visited)) {
        to_visit.push({cost[n], n});
      }
    }
  }

  if (cost[end] == numeric_limits<double>::infinity()) {
    throw std::runtime_error("no valid path found");
  }

  // Reconstruct path
  vector<int> path;
  current = end;
  while (current != start) {
    path.push_back(current);
    current = prev[current];
  }
  path.push_back(start);
  reverse(path.begin(), path.end());

  cout << "Path: ";
  for (auto& id : path) std::cout << id << " ";
  cout << "\n";

  return path;
}

/*
  Minimum spanning tree algorithms
*/

/*
  Prim's algorithm
  Greedy algorithm that finds minimum spanning tree for weighted undirected
  graph
  Only finds minimum spanning tree in a fully connected graph

  1) Create a set mstSet that keeps track of vertices already included in MST.
  2) Assign a key value to all vertices in the input graph. Initialize all key
  values as INFINITE. Assign key value as 0 for the first vertex so that it is
  picked first.
  3) While mstSet doesn’t include all vertices
  ….a) Pick a vertex u which is not there in mstSet and has minimum key value.
  ….b) Include u to mstSet.
  ….c) Update key value of all adjacent vertices of u. To update the key values,
  iterate through all adjacent vertices. For every adjacent vertex v, if weight
  of edge u-v is less than the previous key value of v, update the key value as
  weight of u-v
*/

int find_min_not_included(const vector<double>& costs,
                          const vector<bool>& included) {
  int min_element = 0;
  int min_cost = 9999;
  for (int i = 0; i < costs.size(); ++i) {
    if (included[i]) continue;
    if (costs[i] < min_cost) {
      min_element = i;
      min_cost = costs[i];
    }
  }
  return min_element;
}

void Prim(const Graph_AdjMat& graph) {
  vector<bool> included(graph.get_num_nodes(), 0);
  vector<int> parent(6, -1);
  vector<double> v_costs(graph.get_num_nodes(), 9999);

  v_costs[0] = 0;

  int num_included = false;  // HACK
  while (num_included < graph.get_num_nodes()) {
    int next_v = find_min_not_included(v_costs, included);
    included[next_v] = 1;
    if (parent[next_v] != -1) {
      v_costs[next_v] = v_costs[parent[next_v]] +
                        graph.edge_weight_between(next_v, parent[next_v]);
    }

    vector<int> neighbors = graph.get_neighbors_of(next_v);
    for (auto& n : neighbors) {
      double new_cost = v_costs[next_v] + graph.edge_weight_between(next_v, n);
      if (!included[n] && new_cost < v_costs[n]) {
        v_costs[n] = new_cost;
        parent[n] = next_v;
      }
    }
    num_included++;
  }

  std::cout << "Edges:" << std::endl;
  for (int i = 0; i < parent.size(); ++i) {
    std::cout << "{" << i << ", " << parent[i] << "} " << std::endl;
  }
}

/*
  Kruskal's algorithm for finding minimum spanning trees
  1. create a graph F (a set of trees), where each vertex in the graph is a
  separate tree
  2. create a set S containing all the edges in the graph
  3. while S is nonempty and F is not yet spanning
      remove an edge with minimum weight from S
      if the removed edge connects two different trees then add it to the forest
  F, combining two trees into a single tree
  At the termination of the algorithm, the forest forms a minimum spanning
  forest of the graph. If the graph is connected, the forest has a single
  component and forms a minimum spanning tree
*/

// TODO move from using linear-time sets to logarithmic time disjoint sets
// Disjoint sets doesn't exist in STL, but it does in Boost and it shouldn't be
// too hard to implement
// It consists of Nodes with: rank, id, parent pointer
// MakeSet(x) : if x is not already in set, make one with rank 0, parent pointer
// to itself
// Find(x): go up parent pointers to find root (i.e. set that x belongs in)
//    Along the way, do path compression (flatten the tree) by setting x.parent
//    = FindParent(x.parent)
// Union(x,y): find root of both elements, and if they are in different trees,
// attach the shorter tree to the taller one

void join_sets(vector<int> memberships, int node1, int node2) {
  int set1 = memberships[node1];
  int set2 = memberships[node2];
  for (auto& m : memberships) {
    if (m == set2) m = set1;
  }
}

void Kruskal(const Graph_AdjMat& graph) {
  // Create a set of all edges in the graph, sorted by smallest weight
  using Edge = pair<double, pair<int, int>>;
  vector<Edge> mst_edges;

  // Make a priority queue of the graph's edges, based on cost
  priority_queue<Edge, vector<Edge>, greater<Edge>> edges_queue;
  vector<Edge> edges = graph.get_all_edges();
  for (auto& e : edges) edges_queue.push(e);

  // Put each vertex in a separate set
  vector<int> set_membership(graph.get_num_nodes());
  for (int i = 0; i < graph.get_num_nodes(); ++i) set_membership[i] = i;

  // first edge adds two spanned nodes
  while (!edges_queue.empty() &&
         mst_edges.size() != graph.get_num_nodes() - 1) {
    Edge next_edge = edges_queue.top();
    edges_queue.pop();
    int node1 = next_edge.second.first;
    int node2 = next_edge.second.second;
    if (set_membership[node1] != set_membership[node2]) {
      join_sets(set_membership, node1, node2);
      mst_edges.push_back({next_edge.first, {node1, node2}});
    }
  }

  // Show results
  int total_cost = 0;
  for (auto& e : mst_edges) {
    std::cout << e.second.first << " " << e.second.second << std::endl;
    total_cost += e.first;
  }
  std::cout << "cost: " << total_cost << std::endl;
}

// Check for cycle in _directed_ graph, using DFS
// To detect a back edge, we can keep track of vertices currently in recursion
// stack of function for DFS traversal. If we reach a vertex that is already in
// the recursion stack, then there is a cycle in the tree.

bool _CheckForCycle(const Graph& graph, int visit, vector<int>& visited,
                    vector<int> rec_stack) {
  visited.push_back(visit);
  rec_stack.push_back(visit);

  bool cycle_exists = false;
  vector<int> neighbors = graph.get_neighbors_of(visit);
  for (const auto& n : neighbors) {
    if (node_in_vec(n, rec_stack)) {
      std::cout << "cycle detected at edge: " << visit << " -> " << n
                << std::endl;
      return true;
    } else {
      cycle_exists =
          (cycle_exists || _CheckForCycle(graph, n, visited, rec_stack));
    }
  }
  return cycle_exists;
}

bool CheckForCycle(const Graph& graph) {
  vector<int> visited;
  vector<int> rec_stack;
  if (_CheckForCycle(graph, 0, visited, rec_stack)) {
    std::cout << "cycle exists" << std::endl;
    return true;
  } else {
    std::cout << "no cycle" << std::endl;
    return false;
  }
}

/*
  Topological sort: on a directed graph, a linear ordering of its vertices such
  that for every directed edge uv from vertex u to vertex v, u comes before v in
  the ordering
  Possible iff the graph is a directed acyclic graph

  Basic idea: DFS / post-order traversal while prepending node to topsort list

  e.g. scheduling a sequence of jobs or tasks based on their dependencies

  L ← Empty list that will contain the sorted nodes
  while there are unmarked nodes do
      select an unmarked node n
      visit(n)

  function visit(node n) // DFS / post-order traversal of graph
    if n has a permanent mark then return
    if n has a temporary mark then stop (not a DAG)
    if n is not marked (i.e. has not been visited yet) then
        mark n temporarily
        for each node m with an edge from n to m do
            visit(m)
        mark n permanently
        add n to head of L

  Each node n gets prepended to the output list L only after considering all
  other nodes which depend on n (all descendants of n in the graph)
*/

enum NodeMark { UNMARKED, TEMP, PERMANENT };

void visit(const Graph& graph, int node, vector<NodeMark>& node_marks,
           vector<int>& topsort_nodes) {
  if (node_marks[node] == PERMANENT) {
    return;
  } else if (node_marks[node] == TEMP) {
    throw std::runtime_error("This is not a DAG");
  } else { // UNMARKED
    node_marks[node] = TEMP;
    vector<int> neighbors = graph.get_neighbors_of(node);
    for (auto& n : neighbors) {
      visit(graph, n, node_marks, topsort_nodes);
    }
    node_marks[node] = PERMANENT;
    topsort_nodes.insert(topsort_nodes.begin(), node);
  }
}

void TopologicalSort(const Graph& graph) {
  vector<int> topsort_nodes;
  vector<NodeMark> node_marks(graph.get_num_nodes(), UNMARKED);

  auto unmarked_nodes_exist = [&node_marks]() {
    auto unmarked = find(node_marks.begin(), node_marks.end(), 0);
    return (unmarked != node_marks.end()) ? true : false;
  }

  while (unmarked_nodes_exist()) {
    int idx = unmarked - node_marks.begin();
    visit(graph, idx, node_marks, topsort_nodes);
    unmarked = find(node_marks.begin(), node_marks.end(), 0);
  }
  // Show result
  for (const auto& v : topsort_nodes) std::cout << v << " ";
}

/*
  Algorithms for checking connected-ness of graphs

  Strongly connected (directed): if every vertex is reachable from every other
  vertex

*/

// Count connected components in undirected graph
// Just do BFS or DFS from every unvisited vertex. Every connected component
// should just need one node of its to be selected to search from
void _DfsUtil(const Graph& graph, int node, vector<bool>& visited) {
  visited[node] = true;

  vector<int> neighbors = graph.get_neighbors_of(node);
  for (const auto& n : neighbors) {
    if (visited[n] == false) {
      _DfsUtil(graph, n, visited);
    }
  }
}

int CountConnectedComponents(const Graph& graph) {
  vector<bool> visited(graph.get_num_nodes(), false);
  int num_connected_components = 0;

  auto unvisited = find(visited.begin(), visited.end(), false);
  while (unvisited != visited.end()) {
    int idx = unvisited - visited.begin();
    _DfsUtil(graph, idx, visited);
    unvisited = find(visited.begin(), visited.end(), false);
    num_connected_components++;
  }
  return num_connected_components;
}

/*
  Tarjan's strongly connected components (SCC) algorithm

  Do DFS from every unvisited node, keeping track of:
  * DFS node numbers
  * DFS root (where DFS started when that node got explored)
  * DFS trace/nodes explored (that weren't already visited)

  Side result: order in which strongly connected components are identified is a
  reverse topological sort formed by the components
*/

// TODO replace with std
int find_min_unused_index(const vector<int>& indices) {
  int min_unused = -1;
  for (auto& i : indices) {
    if (i >= min_unused) min_unused = i + 1;
  }
  return min_unused;
}

// Performs DFS of the graph, finds all successors of node v, and reports all
// strongly connected components of that subgraph
void strongconnect(const Graph& graph, int v, stack<int>& scc_stack,
                   vector<int>& indices, vector<int>& roots,
                   vector<bool>& onstack) {
  // std::cout << "strongconnect " << v << std::endl;
  int index = find_min_unused_index(
      indices);  // TODO replace with parameter or static variable
  indices[v] = index;
  roots[v] = index;
  scc_stack.push(v);
  onstack[v] = true;

  vector<int> neighbors = graph.get_neighbors_of(v);
  for (auto n : neighbors) {
    if (indices[n] == -1) {
      // Successor n has not yet been visited; recurse on it
      strongconnect(graph, n, scc_stack, indices, roots, onstack);
      roots[v] = min(roots[v], roots[n]);
    } else if (onstack[n]) {
      // Successor n is in stack and hence in current SCC
      // This is where the algorithm finds {1, 2, 7}
      roots[v] = min(roots[v], indices[n]);
    } else {
      // this node has already been visited and processed: it's already in
      // another SCC
    }
  }

  // If v is a root node, start a new strongly connected component
  // This happens when all recursions have been popped off

  if (roots[v] == indices[v]) {
    std::cout << "{";
    int w = scc_stack.top();
    scc_stack.pop();
    std::cout << w << " ";

    while (w != v) {
      w = scc_stack.top();
      scc_stack.pop();
      onstack[w] = false;
      std::cout << w << " ";
    }
    std::cout << "} ";
  }
}

void ListStronglyConnectedComponents(const Graph& graph) {
  int n_nodes = graph.get_num_nodes();
  vector<int> indices(n_nodes,
                      -1);  // indices all start undefined, first valid one is 0
  vector<int> roots(n_nodes, 0);
  vector<bool> onstack(n_nodes, false);
  stack<int>
      scc_stack;  // stack of nodes explored but not yet committed to a SCC

  for (int i = 0; i < n_nodes; ++i) {
    if (indices[i] == -1) {
      strongconnect(graph, i, scc_stack, indices, roots, onstack);
    }
  }
}

// Bipartite graph: set of graph vertices decomposed into two disjoint sets such
// that no two graph vertices within the same set are adjacent
// Do BFS while alternating between marking nodes red and black.
// If there is an edge between two nodes of the same color, graph is not
// bipartite
bool CheckForBipartiteGraph(const Graph& graph) {
  enum Color { UNMARKED, BLACK, RED };
  map<Color, string> color_names = {
      {UNMARKED, "unmarked"}, {BLACK, "black"}, {RED, "red"}};
  vector<Color> colors(graph.get_num_nodes(), UNMARKED);
  queue<int> to_visit;
  vector<int> visited;

  to_visit.push(0);
  colors[0] = BLACK;
  while (!to_visit.empty()) {
    int current_node = to_visit.front();
    to_visit.pop();
    if (node_in_vec(current_node, visited)) continue;
    visited.push_back(current_node);

    vector<int> neighbors = graph.get_neighbors_of(current_node);
    for (const auto& n : neighbors) {
      if (!node_in_vec(n, visited)) {
        colors[n] = (colors[current_node] == BLACK) ? RED : BLACK;
        to_visit.push(n);
      }
      if (colors[current_node] == colors[n]) {
        std::cout << "Not a bipartite graph" << std::endl;
        return false;
      }
    }
  }
  std::cout << "This is a bipartite graph" << std::endl;
  return true;
}
