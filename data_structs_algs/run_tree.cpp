#include <iostream>
#include "binary_search_tree.h"

int main(int argc, char *argv[]) {
  BinarySearchTree<double> bst;
  bst.insert(5);
  bst.insert(1);
  bst.insert(3);
  bst.insert(7);
  bst.insert(10);
  bst.insert(0);
  bst.insert(4);

  // std::cout << bst.get_node_count() << std::endl;
  // bst.print_values();
  // bst.print_values_nonrecursive();
  // std::cout << bst.get_successor(4) << std::endl;
  std::cout << is_binary_search_tree(bst.root_) << std::endl;

  return 0;
}
