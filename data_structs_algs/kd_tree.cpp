#include "kd_tree.h"

#define CHECK_ANSWER_BRUTE_FORCE

KDTree::KDTree(const std::vector<Point>& elements) {
  n_dims_ = elements[0].size();  // assume all same size
  build_tree(elements);
};

void KDTree::build_tree(std::vector<Point> elements) {
  // TODO replace with median heuristic for adding new elements
  // get cutting dimension
  // find median of point along dimension
  // median-of-median algorithms run in O(n)
  // insert element into tree, delete that point from vector
  for (int i = 0; i < elements.size(); i++) {
    insert(elements[i]);
  }
}

void KDTree::insert(Point new_point) { _insert(new_point, root_); }

void KDTree::_insert(Point new_point, std::unique_ptr<KDTreeNode>& node) {
  int cutting_dim = get_cutting_dim();
  if (!node) {
    node.reset(new KDTreeNode(new_point, get_cutting_dim()));
  } else if (node->point == new_point) {
    throw std::runtime_error("Attempting to add an existing point");
  } else if (node->point[cutting_dim] <= new_point[cutting_dim]) {
    _insert(new_point, node->right);
  } else {
    _insert(new_point, node->left);
  }
};

int KDTree::get_cutting_dim() {
  //  return (cd_++) % n_dims_;
  return rand() % n_dims_;
}

double KDTree::min_in_dim(int dimension) {
  return _min_in_dim(dimension, root_);
}

double KDTree::_min_in_dim(int dimension, std::unique_ptr<KDTreeNode>& node) {
  if (!node) {
    return std::numeric_limits<double>::max();
  } else if (node->cutting_dim == dimension) {
    if (!node->left) {
      return node->point[dimension];
    } else {
      // recurse on left tree
      return _min_in_dim(dimension, node->left);
    }
  } else {
    // recurse on both trees
    return std::min(node->point[dimension],
                    std::min(_min_in_dim(dimension, node->left),
                             _min_in_dim(dimension, node->right)));
  }
};

Point KDTree::nearest_neighbor(const Point& query) {
  double best_dist = std::numeric_limits<double>::max();
  Point best_point = _nearest_neighbor(query, *root_, root_->point, best_dist);

#ifdef CHECK_ANSWER_BRUTE_FORCE
  Point best_point_check =
      _brute_force_nearest_neighbor(query, *root_, root_->point, best_dist);
  if (best_point != best_point_check) {
    std::runtime_error(
        "Nearest neighbor answer doesn't match brute force check");
  }
#endif
  return best_point;
}

Point KDTree::_nearest_neighbor(const Point& query, const KDTreeNode& node,
                                Point best_point, double& best_dist) {
  double node_dist = (node.point - query).norm();
  if (node_dist < best_dist) {
    best_point = node.point;
    best_dist = node_dist;
  }

  // visit subtrees in most promising order (can operate like anytime algorithm)
  // eliminate branches that definitely don't closer points
  int cd = node.cutting_dim;
  double d = query[cd] - node.point[cd];
  bool left_viable = (node.left && -d <= best_dist);
  bool right_viable = (node.right && d <= best_dist);

  if (d < 0) {
    if (left_viable) {
      best_point = _nearest_neighbor(query, *node.left, best_point, best_dist);
    }
    if (right_viable) {
      best_point = _nearest_neighbor(query, *node.right, best_point, best_dist);
    }
  } else {
    if (right_viable) {
      best_point = _nearest_neighbor(query, *node.right, best_point, best_dist);
    }
    if (left_viable) {
      best_point = _nearest_neighbor(query, *node.left, best_point, best_dist);
    }
  }
  return best_point;
}

Point KDTree::_brute_force_nearest_neighbor(const Point& query,
                                            const KDTreeNode& node,
                                            Point best_point,
                                            double& best_dist) {
  double node_dist = (node.point - query).norm();
  if (node_dist < best_dist) {
    best_point = node.point;
    best_dist = node_dist;
  }
  if (node.left) {
    best_point = _nearest_neighbor(query, *node.left, best_point, best_dist);
  }
  if (node.right) {
    best_point = _nearest_neighbor(query, *node.right, best_point, best_dist);
  }
  return best_point;
}

// From Andrew Moore's thesis: "If the point is at a leaf, this is
// straightforward. Otherwise, it is difficult, because the structure of both
// trees below this node are pivoted around the point we wish to remove. One
// solution would be to rebuild the tree below the deleted point, but on
// occasion this would be very expensive. My solution is to mark the point as
// deleted with an extra field in the kd-tree node, and to ignore deletion
// nodes in nearest neighbour and similar searches. When the tree is next
// rebuilt, all deletion nodes are removed."
void KDTree::remove(Point point) { _remove(point, root_); }

void KDTree::_remove(Point point, std::unique_ptr<KDTreeNode>& node) {
  if (!node) {
    throw std::runtime_error("Point not found");
  }
  int cutting_dim = node->cutting_dim;

  if (point == node->point) {  // this is the point to delete
    // TODO deal with deleting here
    if (node->right) {  // if right subtree exists, take min of that and replace

    } else if (node->left) {  // swap subtrees and use min(cd) from new right:

    } else {  // this is just a leaf; delete it
      node = nullptr;
    }
  } else if (point[cutting_dim] > node->point[cutting_dim]) {
    _remove(point, node->right);
  } else {
    _remove(point, node->left);
  }
}

void KDTree::print_tree() { print_node_and_children(root_); }

// TODO come up with better way to do this
void KDTree::print_node_and_children(std::unique_ptr<KDTreeNode>& node) {
  if (node) {
    std::cout << node->point.transpose() << ", " << node->cutting_dim
              << std::endl;
    std::cout << "Left: ";
    print_node_and_children(node->left);
    std::cout << "Right: ";
    ;
    print_node_and_children(node->right);
  } else {
    std::cout << "\n";
  }
}

int main(int argc, char** argv) {
  using Eigen::Vector2d;
  std::vector<Point> points = {Vector2d(-1.0, 1.0), Vector2d(0.0, 0.0),
                               Vector2d(0.5, 0.0),  Vector2d(10.0, 10.0),
                               Vector2d(0.1, 0.3),  Vector2d(0.9, 0.4),
                               Vector2d(0.3, 0.7)};

  KDTree kd_tree(points);
  // std::cout << kd_tree.min_in_dim(0) << std::endl;

  Vector2d query(0.5, 0.0);
  std::cout << "Closest point to " << query.transpose() << std::endl;
  std::cout << kd_tree.nearest_neighbor(query).transpose() << std::endl;

  // kd_tree.print_tree();
  // std::cout <<  bool(kd_tree.root_->right) << std::endl;
}
