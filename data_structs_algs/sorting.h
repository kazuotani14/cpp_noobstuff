#pragma once

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <vector>

using std::swap;

template <typename T>
void print_vec(const std::vector<T>& v) {
  for (const auto& e : v) std::cout << e << " ";
  std::cout << "\n";
}

/*
  Sorting algorithms
  All of these are comparison sorts (vs something like radix sort)
*/

/*
  Merge sort
  Best, average, worst O(n logn)
  Stable

  1. Recursively split into [l, m], [m+1, r]
  2. When size is 1, start merging.

*/
template <typename T>
void merge(std::vector<T>& v, int l, int m, int r) {
  std::vector<T> v1(v.begin() + l, v.begin() + m + 1);
  std::vector<T> v2(v.begin() + m + 1, v.begin() + r + 1);

  int idx1 = 0;
  int idx2 = 0;
  int k = l;

  int size1 = m - l + 1;  // size
  int size2 = r - m;

  while (idx1 < size1 && idx2 < size2) {
    if (v1[idx1] <= v2[idx2]) {
      v[k++] = v1[idx1++];
    } else {
      v[k++] = v2[idx2++];
    }
  }

  // Copy remaining elements, if there are any
  while (idx1 < size1) {
    v[k++] = v1[idx1++];
  }
  while (idx2 < size2) {
    v[k++] = v2[idx2++];
  }
}

template <typename T>
void _MergeSort(std::vector<T>& v, int l, int r) {
  if (l >= r) return;

  int m = l + (r - l) / 2;
  _MergeSort(v, l, m);
  _MergeSort(v, m + 1, r);
  merge(v, l, m, r);
}

template <typename T>
void MergeSort(std::vector<T>& v) {
  _MergeSort(v, 0, v.size() - 1);
}

/*
  Quick sort
  Best, average O(n logn) worst O(n^2)
  Typical version not stable
  When implemented well, can be 2~3 times faster than merge, heap sort
  In-place!

  1. Pick a pivot, move it to the end.
  2. Move along array, moving elements smaller than pivot to index that is
  incremented each time.
      (i.e. the index points to the next position that is guaranteed to be >=
  pivot)
  3. Swap pivot to its correct position (it won't move)
  4. Repeat for both sides of pivot
*/
template <typename T>
void _QuickSort(std::vector<T>& v, int begin, int end) {
  if (begin >= end) return;

  // Move pivot to end
  int pivot = begin + rand() % (end - begin + 1);
  swap(v[pivot], v[end]);

  // Go along array, pushing elements smaller than pivot back
  int pivot_pos = begin;
  for (unsigned int i = begin; i < end; ++i) {
    if (v[end] > v[i]) {
      swap(v[pivot_pos++], v[i]);
    }
  }

  // Move pivot to its final position
  swap(v[pivot_pos], v[end]);

  // Recurse
  _QuickSort(v, begin, pivot_pos - 1);
  _QuickSort(v, pivot_pos + 1, end);
}

template <typename T>
void QuickSort(std::vector<T>& v) {
  _QuickSort(v, 0, v.size() - 1);
}

/*
  Heap sort
  Best, average, worst O(n logn)
  Not stable

  1. Max-heapify the array, so that each node is greater than or equal its
  children.
    Starting from the back of the array (bottom), sift down.
  2. Swap the first and last elements (moving max value to end, in its final
  position). Now a small value is on the top of the tree, so sift it down.
  Repeat while decreasing the "size" of the heap.
*/
