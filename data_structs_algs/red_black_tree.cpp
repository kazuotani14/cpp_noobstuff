// #define DEBUG

template <typename T>
TreeNode<T>* RedBlackTree<T>::Insert(T value) {
  TreeNode<T>* parent = nullptr;
  TreeNode<T>* node;
  TreeNode<T>* new_node;
  if (!root_) {
    root_ = new TreeNode<T>(value, parent);
    node = root_;
    new_node = node;
  } else {
    node = find_position(value, parent);

    if (left_) {
      node->left = new TreeNode<T>(value, parent);
      new_node = node->left;
    } else {
      node->right = new TreeNode<T>(value, parent);
      new_node = node->right;
    }
  }

  insert_repair_tree(new_node);
#ifdef DEBUG
  print();
#endif
  return new_node;
}

template <typename T>
void RedBlackTree<T>::insert_repair_tree(TreeNode<T>* node) {
  if (node == root_) {
#ifdef DEBUG
    std::cout << "case 1: node is root - change to black" << std::endl;
#endif
    insert_case1(node);
  } else if (Parent(node) && Parent(node)->color == BLACK) {
#ifdef DEBUG
    std::cout << "case 2: parent is black - do nothing" << std::endl;
#endif
    insert_case2(node);
  } else if (Uncle(node) &&
             Uncle(node)->color == RED) {  // Uncle==Null means Uncle==BLACK
#ifdef DEBUG
    std::cout << "case 3: parent is red and uncle is red - push blackness down "
                 "from grandparent"
              << std::endl;
#endif
    insert_case3(node);
  } else {
#ifdef DEBUG
    std::cout << "case 4: parent is red and uncle is black - rotate"
              << std::endl;
    insert_case4(node);
#endif
  }
}

template <typename T>
void RedBlackTree<T>::insert_case1(TreeNode<T>* node) {
  node->color = BLACK;
}

template <typename T>
void RedBlackTree<T>::insert_case2(TreeNode<T>* node) {
  return;
}

template <typename T>
void RedBlackTree<T>::insert_case3(TreeNode<T>* node) {
  Parent(node)->color = BLACK;
  Uncle(node)->color = BLACK;
  TreeNode<T>* g = Grandparent(node);
  g->color = RED;
  insert_repair_tree(g);
}

template <typename T>
void RedBlackTree<T>::insert_case4(TreeNode<T>* node) {
  // Step 1
  TreeNode<T>* p = Parent(node);
  TreeNode<T>* g = Grandparent(node);

  if (g && g->left && node == g->left->right) {
    RotateLeft(p);
    node = node->left;
  } else if (g && g->right && node == g->right->left) {
    RotateRight(p);
    node = node->right;
  }

  // Step 2
  p = Parent(node);
  g = Grandparent(node);
  if (p && p->left && node == p->left) {
    RotateRight(g);
  } else {
    RotateLeft(g);
  }
  p->color = BLACK;
  g->color = RED;
}

template <typename T>
void RedBlackTree<T>::Remove(T value) {
  TreeNode<T>* pos = find_position(value);
  if (pos && pos->data==value) {
    if(!pos->left && pos->right) delete pos;
    else remove_has_child(pos);
  }
  else {
    throw runtime_error("value does not exist");
  }
}

template <typename T>
void RedBlackTree<T>::remove_has_child(TreeNode<T>* node){
  cout << "test" << endl;
}

template <typename T>
T RedBlackTree<T>::lower_bound(T value) {
  int lb = 999;
  _lower_bound(root_, lb, value);
  if(lb < value) return lb;
  else throw runtime_error("lower bound does not exist");
}

template <typename T>
void RedBlackTree<T>::_lower_bound(TreeNode<T>* node, T& lb, T value){
  if(!node) return;
  if(node->data >= value) {
    _lower_bound(node->left, lb, value);
  }
  else {
    lb = node->data;
   _lower_bound(node->right, lb, value);
  }
}

template <typename T>
T RedBlackTree<T>::upper_bound(T value) {
  int ub = -999;
  _upper_bound(root_, ub, value);
  if(ub >= value) return ub;
  else throw runtime_error("upper bound does not exist");
  return 0;
}

template <typename T>
void RedBlackTree<T>::_upper_bound(TreeNode<T>* node, T& ub, T value) {
  if(!node) return;
  if(node->data >= value) {
    ub = node->data;
    _upper_bound(node->left, ub, value);
  }
  else {
   _upper_bound(node->right, ub, value);
  }
}

template <typename T>
TreeNode<T>* RedBlackTree<T>::Parent(TreeNode<T>* n) {
  return n->parent;
}

template <typename T>
TreeNode<T>* RedBlackTree<T>::Grandparent(TreeNode<T>* n) {
  TreeNode<T>* p = n->parent;
  return (!p) ? p : p->parent;
}

template <typename T>
TreeNode<T>* RedBlackTree<T>::Sibling(TreeNode<T>* n) {
  TreeNode<T>* p = Parent(n);
  if (!p) return p;
  return (p->left == n) ? p->right : p->left;
}

template <typename T>
TreeNode<T>* RedBlackTree<T>::Uncle(TreeNode<T>* n) {
  TreeNode<T>* p = Parent(n);
  TreeNode<T>* g = Grandparent(n);
  if (!g) return nullptr;
  return Sibling(p);
}

template <typename T>
void RedBlackTree<T>::RotateLeft(TreeNode<T>* n) {
  std::cout << "rotate left at " << n->data << std::endl;

  TreeNode<T>* nnew = n->right;
  if (!nnew) {
    throw runtime_error(
        "since the leaves of a red-black tree are empty, they cannot become "
        "internal nodes");
  }

  n->right = nnew->left;
  nnew->left = n;
  nnew->parent = n->parent;
  n->parent = nnew;

  if (nnew->parent) {
    if (nnew->parent->right == n)
      nnew->parent->right = nnew;
    else
      nnew->parent->left = nnew;
  }

  if (n == root_) root_ = nnew;
}

template <typename T>
void RedBlackTree<T>::RotateRight(TreeNode<T>* n) {
  std::cout << "rotate right at " << n->data << std::endl;

  TreeNode<T>* nnew = n->left;
  if (!nnew) {
    throw runtime_error(
        "since the leaves of a red-black tree are empty, they cannot become "
        "internal nodes");
  }

  n->left = nnew->right;
  nnew->right = n;
  nnew->parent = n->parent;
  n->parent = nnew;

  // update downward pointing relationships
  if (nnew->parent->right == n)
    nnew->parent->right = nnew;
  else
    nnew->parent->left = nnew;
  if (n->parent->right == n)
    nnew->right = n;
  else
    nnew->left = n;

  if (n == root_) root_ = nnew;
}

template <typename T>
bool RedBlackTree<T>::is_in_tree(T value) {
  TreeNode<T>* parent;
  TreeNode<T>*& pos = find_position(value, parent);
  return pos ? true : false;
}

template <typename T>
T RedBlackTree<T>::get_min() {
  TreeNode<T>* node = root_;
  while (node->left) {
    node = node->left;
  }
  return node->data;
}

template <typename T>
T RedBlackTree<T>::get_max() {
  TreeNode<T>* node = root_;
  while (node->right) {
    node = node->right;
  }
  return node->data;
}

template <typename T>
TreeNode<T>* RedBlackTree<T>::find_position(T value, TreeNode<T>*& parent) {
  parent = nullptr;
  return _find_position(value, root_, parent);
}

template <typename T>
TreeNode<T>* RedBlackTree<T>::find_position(T value) {
  TreeNode<T>* parent;
  return find_position(value, parent);
}

template <typename T>
TreeNode<T>* RedBlackTree<T>::_find_position(T value, TreeNode<T>* node,
                                             TreeNode<T>*& parent) {
  if (node->data >= value) {
    parent = node;
    if (!node->left) {
      left_ = true;
      return node;
    } else
      return _find_position(value, node->left, parent);
  } else {
    parent = node;
    if (!node->right) {
      left_ = false;
      return node;
    } else
      return _find_position(value, node->right, parent);
  }
}

template <typename T>
int RedBlackTree<T>::get_height() {
  return _get_height(0, root_);
}

template <typename T>
int RedBlackTree<T>::_get_height(int height, TreeNode<T>* node) {
  if (node) {
    return max(_get_height(height + 1, node->left),
               _get_height(height + 1, node->right));
  } else {
    return height;
  }
}

template <typename T>
int RedBlackTree<T>::get_node_count() {
  return _get_node_count(root_);
}

template <typename T>
int RedBlackTree<T>::_get_node_count(TreeNode<T>* node) {
  if (node) {
    return 1 + _get_node_count(node->left) + _get_node_count(node->right);
  } else {
    return 0;
  }
}

template <typename T>
void RedBlackTree<T>::print() {
  cout << "---" << endl;
  _print_values(root_, 0);
  cout << "\n";
}

// in-order traversal
template <typename T>
void RedBlackTree<T>::_print_values(TreeNode<T>* node, int depth) {
  if (node) {
    _print_values(node->left, depth + 1);
    // cout << node->data << " ";
    cout << node->data << " at layer " << depth << ", "
         << ColorNames[node->color] << endl;
    _print_values(node->right, depth + 1);
  }
}
