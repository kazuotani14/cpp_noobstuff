/*
  k-dimensional tree (kd-tree) implementation for nearest-neighbors
    * Each level has a “cutting dimension”
    * Cycle through the dimensions as you walk down the tree.
    * Each node contains a point and cutting dimension

    * End up with a binary tree
    – Size: O(N)
    – Depth: O(log N)
    – Construction time: O(N log N)

    * Worst case query complexity: O(N)
    • In practice, query complexity is closer to:
      - O(2d + log n)
      - log n to find cells “near” the query point
      - 2d to search around cells in that neighborhood

    Ideas:
    - storing partial results: keep best so far, and update
    - pruning: reduce search space by eliminating irrelevant trees.
    - traversal order: visit the most promising subtree first.

  Notes:
  * kd-trees are almost equivalent to brute force in high dimensions. See
  https://stackoverflow.com/questions/5751114/nearest-neighbors-in-high-dimensional-data
  for other methods (e.g. locally sensitive hashing: hash method that stores
  points from high-dimensional space that are close into same bin)
  * Voronoi diagrams and linear search are pretty much all what known for exact
  algorithms with theoretical guarantees. Even kd-trees are brute-force search
  in worst case
  * chapter from Andrew Moore's thesis:
  http://www.ri.cmu.edu/pub_files/pub1/moore_andrew_1991_1/moore_andrew_1991_1.pdf
*/

/*
TODO
  * implement other basic functionality: remove, k nearest neighbors (use
priority queue with size-checking?)

  * Build tree with entire data set given
    * trick for keeping tree balanced: get median of points being put into
subtree wrt dimension
  * add notion of hyperrectangle for elimination rather than just cutting plane
    * two arrays: min and max (start all with -inf, inf)

  * extend to quadtree (compare two dimensions at once - useful for 2d), octrees
(3 dims at once)
  * investigate speedups with multi-threading / parallel search thru tree
*/

#ifndef _KDTREE_H_
#define _KDTREE_H_

#include <algorithm>
#include <cstdlib>
#include <eigen/Eigen/Core>
#include <exception>
#include <iostream>
#include <limits>
#include <memory>
#include <vector>

typedef Eigen::VectorXd Point;

class KDTree {
 private:
  struct KDTreeNode {
    KDTreeNode(const Point& vec, int dim) : point(vec), cutting_dim(dim){};

    Point point;
    int cutting_dim;
    std::unique_ptr<KDTreeNode> left;  // if(left) to check if assigned
    std::unique_ptr<KDTreeNode> right;
  };

 public:
  KDTree(){};
  KDTree(const std::vector<Point>& elements);

  void insert(Point new_point);
  void remove(Point point);

  double min_in_dim(int dimension);
  Point nearest_neighbor(const Point& query);
  Point k_nearest_neighbors(const Point& vec, int n);

  // std::vector<Point> range_query(const Point& lower, const Point& upper);

  void print_tree();

 private:
  std::unique_ptr<KDTreeNode> root_;
  int n_dims_;
  int cd_ = 0;
  int get_cutting_dim();

  void build_tree(std::vector<Point> elements);

  void _insert(Point new_point, std::unique_ptr<KDTreeNode>& node);
  void _remove(Point point, std::unique_ptr<KDTreeNode>& node);
  double _min_in_dim(int dimension, std::unique_ptr<KDTreeNode>& node);
  Point _nearest_neighbor(const Point& query, const KDTreeNode& node,
                          Point best_point, double& best_dist);
  Point _brute_force_nearest_neighbor(const Point& query,
                                      const KDTreeNode& node, Point best_point,
                                      double& best_dist);

  void print_node_and_children(std::unique_ptr<KDTreeNode>& node);
};

#endif
