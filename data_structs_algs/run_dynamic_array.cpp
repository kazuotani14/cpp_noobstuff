#include "dynamic_array.h"

int main(int argc, char** argv) {
  kazulib::Vector<int> vec;

  vec.push_back(0);
  vec.push_back(1);
  vec.push_back(2);

  print_vector(vec);

  vec.insert(0, -1);
  print_vector(vec);

  vec.delete_by_index(0);
  print_vector(vec);

  for (int i = 5; i < 10; i++) {
    vec.push_back(i);
  }
  print_vector(vec);
  vec.remove(5);
  print_vector(vec);

  kazulib::Vector<int> vec2;
  vec2.push_back(10);
  vec2.push_back(9);
  vec = vec2;
  print_vector(vec);
  print_vector(vec2);
  // std::cout << vec.size() << std::endl;

  return 0;
}
