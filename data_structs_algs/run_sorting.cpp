#include "sorting.h"

#include <chrono>

int main(int argc, char** argv) {
  int size = 5000;

  std::vector<int> vec1;
  for (unsigned int i = 0; i < size; ++i) {
    vec1.push_back(rand() % (size * 3));
  }
  // vec1.push_back(5);
  std::vector<int> vec2 = vec1;

  // print_vec(vec1);
  auto start = std::chrono::system_clock::now();
  MergeSort(vec2);
  auto now = std::chrono::system_clock::now();
  long int elapsed =
      std::chrono::duration_cast<std::chrono::milliseconds>(now - start)
          .count();
  std::cout << "merge sort (shitty implementation): " << elapsed << "ms"
            << std::endl;
  // print_vec(vec1);

  // print_vec(vec2);
  start = std::chrono::system_clock::now();
  QuickSort(vec1);
  now = std::chrono::system_clock::now();
  elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(now - start)
                .count();
  std::cout << "quick sort: " << elapsed << "ms" << std::endl;
  // print_vec(vec2);

  return 0;
}
