#ifndef _LINKEDLIST_H_
#define _LINKEDLIST_H_

#include <cstdio>
#include <exception>
#include <iostream>

// Note for linked list implementation: when traversing, try to keep track of
// the element you want to modify in the end

namespace kazulib {

// TODO find better ways to traverse list with less edge case checks
// in: insert, find, erase, remove_value

using std::size_t;

template <typename T>
class LinkedList {
  struct Node {
    Node(T value) : element(value) {}
    T element;
    Node* next = nullptr;
  };

 public:
  LinkedList() {}

  LinkedList(T value) {
    head_ = new Node(value);
    tail_ = head_;
  }

  bool empty() { return (head_ == nullptr); }

  void push_front(T value) {
    Node* new_node = new Node(value);
    new_node->next = head_;
    head_ = new_node;
  }

  T pop_front() {
    if (!empty()) {
      T value = head_->element;
      Node* to_delete = head_;
      head_ = to_delete->next;
      delete to_delete;
      return value;
    } else {
      throw std::runtime_error("empty list");
    }
  }

  void push_back(T value) {
    if (head_ == nullptr) {
      head_ = new Node(value);
      tail_ = head_;
    } else {
      tail_->next = new Node(value);
      tail_ = tail_->next;
    }
  }

  T pop_back() {
    Node* place;
    if (head_ == tail_) {
      place = head_;
    } else {
      place = head_;
      while (place->next != tail_) {  // stop before tail
        place = place->next;
      }
    }
    T value = tail_->element;
    delete tail_;
    place->next = nullptr;
    tail_ = place;
    return value;
  }

  T front() {
    if (!empty()) {
      return head_->element;
    } else {
      throw std::runtime_error("empty list");
    }
  }

  T back() {
    if (!empty()) {
      return tail_;
    } else {
      throw std::runtime_error("empty list");
    }
  }

  T value_at(int index) {
    if (index < 0) {
      throw std::runtime_error("invalid index");
    }
    int i = 0;
    Node* place = head_;
    while (i++ != index) {
      place = place->next;
      if (place == nullptr) {
        throw std::runtime_error("invalid index");
      }
    }
    return place->element;
  }

  void insert(int index, T value) {
    if (index == 0) {
      Node* current = head_;
      head_ = new Node(value);
      head_->next = current;
    } else {
      Node* place = head_;
      int i = 0;
      while (++i != index)  // place will be at element before index
      {
        place = place->next;
        if (place == nullptr) {
          throw std::runtime_error("invalid index");
        }
      }
      Node* current = place->next;
      place->next = new Node(value);
      place->next->next = current;
      if (place == tail_) tail_ = place->next;
    }
  }

  int find(T value)  // find first occurence of value
  {
    Node* place = head_;
    int i = 0;
    while (place->element != value) {
      i++;
      place = place->next;
    }
    if (place != nullptr)
      return i;
    else
      return -1;
  }

  void erase(int index)  // removes node at given index
  {
    if (index == 0) {
      Node* to_delete = head_;
      head_ = head_->next;
      delete to_delete;
    } else {
      Node* place = head_;
      int i = 0;
      while (++i != index)  // place will be at element before index
      {
        place = place->next;
        if (place == nullptr) {
          throw std::runtime_error("invalid index");
        }
      }
      Node* to_delete = place->next;
      if (to_delete == tail_) tail_ = place;
      place->next = to_delete->next;
      delete to_delete;
    }
  }

  void remove_value(
      T value)  // removes the first item in the list with this value
  {
    Node* place = head_;
    Node* prev = nullptr;
    while (place->element != value) {
      prev = place;
      place = place->next;
    }
    if (place == nullptr) throw std::runtime_error("failed to find value");
    if (prev == nullptr) {
      Node* to_delete = head_;
      head_ = head_->next;
      delete to_delete;
    } else {
      std::cout << "to delete: " << place->element << std::endl;
      Node* to_delete = place;
      if (to_delete == tail_) tail_ = prev;
      prev->next = place->next;
      delete to_delete;
    }
  }

  // reverses the list
  // go forward thru list while saving last visited - point node->next towards
  // previous
  void reverse() {
    Node* place = head_;
    Node* prev = nullptr;
    tail_ = head_;
    while (place != nullptr) {
      Node* next_place = place->next;
      place->next = prev;
      prev = place;
      place = next_place;
    }
    head_ = prev;
  }

  void print() {
    Node* place = head_;
    while (place != nullptr) {
      std::cout << place->element << " ";
      place = place->next;
    }
    std::cout << "\n";
  }

 private:
  Node* head_ = nullptr;
  Node* tail_ = nullptr;
};

}  // namespace kazulib

#endif
