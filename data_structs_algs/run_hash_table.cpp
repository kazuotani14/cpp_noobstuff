#include "hash_table.h"

#include <iostream>
#include <string>

int main(int argc, char *argv[]) {
  HashTable<std::string, double> htable(10);

  htable.add("test", 0);
  htable.add("this", 5);
  htable.add("works", 10);

  std::cout << htable.exists("test") << std::endl;
  htable.remove("test");
  std::cout << htable.exists("test") << std::endl;
  std::cout << htable.get("this") << std::endl;

  return 0;
}
