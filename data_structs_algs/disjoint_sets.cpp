/*
  Disjoint sets: keep track of sets that are "disjoint", i.e. not overlapping

  * when doing Union on two sets, put shorter tree under root of taller tree (Union-by-rank)
  * TODO Use path compression to make lookup faster - every time a child is looked up, set its parent to be directly the root of the tree

  Note: this is kind of a quick dirty implementation..
*/

#include <algorithm>
#include <iostream>
#include <stack>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

using namespace std;

int root(int node, vector<int>& parents) {
  if(parents[node] != node) {
    return root(parents[node], parents);
  }
  return parents[node];
}

vector<int> parents(n+1);
for(int i=0; i<parents.size(); ++i) parents[i] = i;
for(auto& e : edges) {
  int u = e[0], v = e[1], pu = root(u, parents);
  if(v == pu) {
    if(candA.empty()) return e;
    else return candA;
  }
  parents[v] = u;
}

/*
  --------------------
  Stuff below is old, literal implemenation. Better implementation above
  --------------------
*/

class DisjointSetElement {
public:
  DisjointSetElement(int val, DisjointSetElement* p=nullptr) : val(val), parent(p) {}
  DisjointSetElement* parent = nullptr;
  int val;
  int height = 1;

  DisjointSetElement* findParent() {
    DisjointSetElement* trace = this;
    while(trace->parent != nullptr) {
      trace = trace->parent;
    }
    return trace;
  }

  void setParent(DisjointSetElement* new_parent) {
    parent = new_parent;
  }

  int get_height() {
    return height;
  }
};


class DisjointSets {
public:
  unordered_map<int, DisjointSetElement*> elements;

  void Add(int val) {
    if(!elements[val])
      elements[val] = new DisjointSetElement(val);
    else
      cout << "no can do" << endl;
  }

  void AddToSet(int val, int set_val) {
    if(!elements[set_val]){
      cout << "no can do" << endl;
    }
    else {
      DisjointSetElement* s = elements[set_val];
      if(!elements[val])
        elements[val] = new DisjointSetElement(val, s);
    }
  }

  void Union(int n1, int n2) {
    if(!elements[n1] || !elements[n2]) {
      cout << "no can do" << endl;
      return;
    }
    // find parent of both
    DisjointSetElement* p1 = elements[n1]->findParent();
    DisjointSetElement* p2 = elements[n2]->findParent();

    // put one under the other
    if(p1->get_height() > p2->get_height()) {
      p1->setParent(p2);
    }
    else if(p1->get_height() < p2->get_height()) {
      p2->setParent(p1);
    }
    else { // equal
      p2->setParent(p1);
      p1->height = 1 + p2->get_height();
    }
  }

  bool InSameSet(int n1, int n2) {
    if(!elements[n1] || !elements[n2]) {
      cout << "no can do" << endl;
      return false;
    }
    DisjointSetElement* p1 = elements[n1]->findParent();
    DisjointSetElement* p2 = elements[n2]->findParent();
    if(p1==p2) return true;
    else return false;
  }

};




int main() {
  DisjointSets sets;
  sets.Add(1);
  sets.Add(2);
  sets.AddToSet(3, 2);
  sets.Union(1,2);
  cout << sets.InSameSet(1,2) << endl;
  cout << sets.InSameSet(2,3) << endl;
}
