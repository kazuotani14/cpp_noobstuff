#include "linked_list.h"

int main(int argc, char** argv) {
  kazulib::LinkedList<double> list(5.0);
  list.push_back(0.0);
  list.push_back(3.0);
  list.push_back(1.0);
  list.print();
  // std::cout << "front: " << list.first_->element << " tail: " <<
  // list.tail_->element << std::endl;
  std::cout << list.pop_front() << std::endl;
  std::cout << list.value_at(0) << std::endl;
  // list.print();
  list.insert(3, 11);
  list.print();
  // std::cout << list.find(5) << std::endl;
  // list.erase(0);
  // list.print();
  // list.remove_value(11);
  // list.print();
  // list.remove_value(0);
  list.reverse();
  list.print();
}
