#include "graphs.h"

vector<pair<pair<int, int>, double>> make_grid_edges(int grid_size) {
  vector<pair<pair<int, int>, double>> grid_edges;
  auto grid_index = [grid_size](int row, int col) {
    return grid_size * row + col;
  };

  for (int i = 0; i < grid_size; ++i) {
    for (int j = 0; j < grid_size; ++j) {
      if (i > 0)
        grid_edges.push_back({{grid_index(i, j), grid_index(i - 1, j)}, 1});
      if (j > 0)
        grid_edges.push_back({{grid_index(i, j), grid_index(i, j - 1)}, 1});
      if (i < grid_size - 1)
        grid_edges.push_back({{grid_index(i, j), grid_index(i + 1, j)}, 1});
      if (j < grid_size - 1)
        grid_edges.push_back({{grid_index(i, j), grid_index(i, j + 1)}, 1});
    }
  }
  return grid_edges;
}

int main(int argc, char* argv[]) {
  using namespace std;

  // vector<pair<int, int>> edges = { {0, 1}, {0, 5}, {2, 3}, {2, 5}, {4, 5} };
  // //tree
  // vector<pair<int, int>> edges = { {0, 1}, {1, 2}, {1, 5}, {2, 3}, {3, 5},
  // {4, 5} }; // w cycle
  //
  // Graph_AdjList graph1(6);
  // graph1.add_edges(edges);
  // Graph_AdjMat graph2(6);
  // graph2.add_edges(edges);

  // DepthFirstSearchIterative(graph1, 0);
  // DepthFirstSearchIterative(graph2, 0);
  // DepthFirstSearchRecursive(graph1, 0);
  // DepthFirstSearchRecursive(graph2, 0);
  //
  // BreadthFirstSearchIterative(graph1, 0);
  // BreadthFirstSearchIterative(graph2, 0);

  // Graph_AdjMat grid_graph(5 * 5);
  // grid_graph.add_edges_weighted(make_grid_edges(5));
  // std::cout << grid_graph.get_num_nodes() << std::endl;
  // Dijkstra(grid_graph, 0, 8);
  // Dijkstra(grid_graph, 8, 0);

  vector<pair<pair<int, int>, double>> edges1 = {
      {{0, 1}, 1}, {{1, 2}, 1}, {{5, 1}, 2}, {{2, 3}, 2},
      {{2, 4}, 5}, {{3, 5}, 3}, {{5, 4}, 1}};  // w cycle

  vector<pair<pair<int, int>, double>> edges2 = {
      {{0, 1}, 1}, {{1, 2}, 1}, {{1, 5}, 2},
      {{2, 3}, 2}, {{5, 4}, 1}, {{5, 3}, 1}};  // without cycle

  Graph_AdjMat graph3(6);
  graph3.add_edges_weighted(edges1);
  Graph_AdjMat graph4(6);
  graph4.add_edges_weighted(edges2);

  // Prim(graph3);
  // Kruskal(graph3);
  // CheckForCycle(graph3); // cycle
  // CheckForCycle(graph4); // no cycle
  // TopologicalSort(graph3); // should fail
  // TopologicalSort(graph4);

  // vector<pair<int, int>> edges3 = {
  //   {0, 1}, {1, 2}, {2, 0}, {3, 1}, {3, 2}, {3, 4}, {4, 3},
  //   {5, 2}, {7, 4}, {7, 7}, {7, 6}, {5, 6}, {6,5}
  // };
  vector<pair<int, int>> edges3 = {{7, 1}, {1, 2}, {2, 7}, {3, 1}, {3, 2},
                                   {3, 4}, {4, 3}, {5, 2}, {0, 4}, {0, 0},
                                   {0, 6}, {5, 6}, {6, 5}};
  Graph_AdjMat_Directed graph5(8);
  graph5.add_edges(edges3);
  ListStronglyConnectedComponents(graph5);
  // expect {0, 1, 2}, {3, 4}, {5, 6}, {7}
  // Notice how the output order is reverse topological sort of SCCs

  vector<pair<int, int>> edges4 = {{0, 1}, {1, 2}, {3, 4}};
  Graph_AdjMat graph6(5);
  graph6.add_edges(edges4);
  // std::cout << "# of connected components: " <<
  // CountConnectedComponents(graph6) << std::endl;

  vector<pair<int, int>> edges5 = {// bipartite
                                   {0, 3},
                                   {1, 3},
                                   {1, 4},
                                   {2, 4}};
  Graph_AdjMat graph7(5);
  vector<pair<int, int>> edges6 = {// bipartite
                                   {0, 3},
                                   {1, 3},
                                   {1, 4},
                                   {2, 4},
                                   {1, 2}};
  Graph_AdjMat graph8(5);
  graph8.add_edges(edges6);
  // CheckForBipartiteGraph(graph7); // expect true
  // CheckForBipartiteGraph(graph8); // expect false

  return 0;
}
