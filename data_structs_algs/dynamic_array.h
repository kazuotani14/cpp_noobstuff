#ifndef _DYNAMICARRAY_H_
#define _DYNAMICARRAY_H_

#include <exception>
#include <iostream>

namespace kazulib {

template <typename T>
class Vector {
 public:
  Vector() {
    array_ = new T[initial_capacity_];
    size_ = 0;
    capacity_ = initial_capacity_;
  }

  Vector(T* input, int n) {
    array_ = new T[n];
    size_ = n;
    capacity_ = n;
    for (int i = 0; i < n; i++) {
      *(array_ + i) = *(input + n);
    }
  }

  void push_back(T element) {
    if (size_ == capacity_) {
      T* new_array = new T[capacity_ * 2];
      capacity_ *= 2;

      memcpy(new_array, array_, sizeof(element) * size_);
      *(new_array + size_) = element;

      delete[] array_;
      array_ = new_array;
    } else {
      *(array_ + size_) = element;
    }
    size_++;
  }

  // make vector larger or smaller as necessary
  void resize(int n) {
    if (n > size_ && n < capacity_) {
      // do nothing
    } else if (n < size_ && n > 0) {
      // make array smaller, chop off extra elements
      T* new_array = new T[n];
      capacity_ = n;
      size_ = n;
      memcpy(new_array, array_, sizeof(*array_) * n);
    } else if (n > capacity_) {
      // make array larger
      T* new_array = new T[capacity_ * 2];
      capacity_ *= 2;
      memcpy(new_array, array_, sizeof(*array_) * size_);
    } else {
      throw std::runtime_error("Invalid desired size");
    }
  }

  T front() {
    if (size_ > 0) {
      return array_[0];
    } else {
      throw std::runtime_error("empty array - can't return front");
    }
  }

  T back() {
    if (size_ > 0) {
      return *(array_ + size_ - 1);
    } else {
      throw std::runtime_error("empty array - can't return front");
    }
  }

  // TODO if size is 1/4 of capacity, resize to half
  T pop() {
    if (!empty()) {
      T popped = *(array_ + (size_ - 1));
      size_--;
      return popped;
    } else {
      throw std::runtime_error("empty array - can't pop");
    }
  }

  T* data() { return array_; }

  void insert(int idx, T element) {
    if (idx < 0 || idx > size_) {
      throw std::runtime_error("invalid insertion index");
    }
    if (size_ == capacity_) {
      resize(capacity_ * 2);
    }

    memcpy(array_ + idx + 1, array_ + idx, (size_ - idx) * sizeof(*array_));
    *(array_ + idx) = element;
  }

  // delete element at index
  void delete_by_index(int idx) {
    if (idx < size_ && idx >= 0 && idx < size_) {
      memcpy(array_ + idx, array_ + (idx + 1), (size_ - idx) * sizeof(*array_));
    } else {
      throw std::runtime_error("invalid deletion index");
    }
    size_--;
  }

  void remove(T element) {
    for (int i = 0; i < size_; i++) {
      if (*(array_ + i) == element) {
        delete_by_index(i);
      }
    }
  }

  // return index of first element, or -1 if none
  int find(T element) {
    int i;
    for (i = 0; i < size_; i++) {
      if (*(array_ + i) == element) return i;
    }
    if (i == size_) {
      return -1;
    }
  }

  bool empty() { return (size_ == 0); }

  int size() { return size_; }

  void clear() { size_ = 0; }

  T at(int idx) {
    if (idx >= 0 && idx < size_) {
      return *(array_ + idx);
    } else {
      throw std::runtime_error("Index out of range");
    }
  }

  // access element - throw error if out of range;
  T operator[](int idx) { return this->at(idx); }
  void operator=(Vector<T>& rhs) {
    if (rhs.size() > capacity_) {
      resize(rhs.size());
    }
    size_ = rhs.size();
    for (int i = 0; i < size_; i++) {
      array_[i] = rhs[i];
    }
    // TODO get this memcpy working
    // memcpy(array_, &rhs, rhs.size()*sizeof(rhs[0]));
  }

 private:
  T* array_;
  int size_;
  int capacity_;
  int initial_capacity_ = 2;
};

template <typename T>
void print_vector(kazulib::Vector<T>& vec) {
  for (int i = 0; i < vec.size(); i++) {
    std::cout << vec[i] << " ";
  }
  std::cout << "\n";
}

}  // namespace kazulib

#endif
