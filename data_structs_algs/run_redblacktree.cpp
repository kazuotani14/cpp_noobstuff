#include "red_black_tree.h"

using std::cout;
using std::endl;

int main(int argc, char* argv[]) {
  RedBlackTree<int> bst;
  // bst.insert(3);
  // bst.insert(1);
  // bst.insert(5);
  // bst.insert(7);
  // TreeNode<int>* ptr4 = bst.insert(4);

  // std::cout << bst.Grandparent(ptr4)->data << std::endl;

  vector<int> failure_case{7, 9, 3, 8, 0, 2, 4, 8, 3, 9};
  for (auto& i : failure_case) {
    // cout << "** Insert " << i << endl;
    bst.Insert(i);
  }
  // cout << bst.lower_bound(5) << endl;
  // cout << bst.upper_bound(5) << endl;

  bst.Remove(3);

  // for(int i=0; i<20; ++i) {
  //   int to_insert = rand()%10;
  //   std::cout << "Insert " << to_insert << std::endl;
  //   bst.insert(to_insert);
  // }

  // bst.print();
  // cout << "---\n" << endl;

  // NodePtr<int> pos = bst.find_position(5);
  // bst.rotate_left(pos);
  // bst.print();
  // bst.print_tree();

  // cout << bst.Parent(pos).get()->data << endl; // 5
  // cout << bst.Grandparent(pos).get()->data << endl; //
  // cout << bst.Sibling(pos).get()->data << endl; // 4
  // cout << bst.Uncle(pos).get()->data << endl; // 1

  // cout << bst.upper_bound(3) << endl;

  return 0;
}
