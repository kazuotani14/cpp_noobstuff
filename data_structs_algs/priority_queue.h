#ifndef _PRIORITYQUEUE_H_
#define _PRIORITYQUEUE_H_

#include <algorithm>  //for swap
#include <cstdlib>
#include <exception>
#include <iostream>
#include <vector>

/*
Min-heap (each node is smaller than its children)
Stored in array (tree with indices)

* insert: put at end, then sift up
* pop_min: swap first (min) and last, shorter array and sift_down(new first)
* heapify: starting from last element and going to first, sift_down(element)
* heap sort: pop_min multiple times

*/

/*
  PriorityQueue class
  min-heap implementation
    O(n logn) initial sort
    O(log n) insert and remove
  type T must have operator for comparisons > , <, ==
*/
template <class T>
class PriorityQueue {
 public:
  PriorityQueue(){};

  PriorityQueue(const std::vector<T>& elements) : array_(elements) {
    // Build heap
    // Intuition for starting from last element/bottom: to "sweep" up and make
    // sure we catch everything. If we went top-down, everything that got
    // swapped up wouldn't get checked again
    for (int i = array_.size() - 1; i >= 0; i--) {
      sift_down(i, array_.size() - 1, array_);
    }
  };

  // Note: keeping front() and pop() separate here to have same interface as
  // std::queue
  // Reason:
  // Impossible to implement an exception safe version (with the strong
  // guarantee) for a version of pop which returns a value. i.e. if trying to
  // return the front element throws, element is lost
  const T min() const {
    if (array_.size() > 0) {
      return array_[0];
    } else {
      throw std::runtime_error("MinHeap has no elements");
    }
  };

  void pop_min() {
    if (array_.size() > 0) {
      std::swap(array_[0], array_[array_.size() - 1]);
      array_.resize(array_.size() - 1);
      sift_down(0, array_.size() - 1, array_);
    } else {
      throw std::runtime_error("MinHeap has no elements");
    }
  };

  void insert(T new_element) {
    // Sift new element up until heap property is satisfied
    // Sift up instead of down because:
    // sifting down would imply inserting at zero, which would break tree
    array_.resize(array_.size() + 1);

    int last_idx = array_.size() - 1;
    array_[last_idx] = new_element;
    sift_up(last_idx, array_);
  }

  std::size_t size() const { return array_.size(); };

  void print_array() {
    for (int i = 0; i < array_.size(); i++) {
      std::cout << array_[i] << " ";
    }
    std::cout << "\n";
  }

  bool is_empty() { return (array_.size() == 0); };
  int get_size() { return array_.size(); };

  // Sort using min-heap
  // Ideally we would use max-heap, but here I'll just use min-heap, then
  // reverse
  std::vector<T> heap_sort() {
    std::vector<T> vec = array_;
    for (const auto& i : vec) {
      std::cout << i << " ";
    }
    std::cout << "\n";

    for (int i = vec.size() - 1; i > 0; --i) {
      std::swap(vec[0], vec[i]);
      sift_down(0, i - 1, vec);
    }
    std::reverse(vec.begin(), vec.end());
    return vec;
  }

 private:
  std::vector<T> array_;

  void sift_up(int i, std::vector<T>& vec) {
    while (i != 0) {
      int parent = (i % 2 != 0) ? (i - 1) / 2 : (i - 2) / 2;
      if (vec[parent] > vec[i]) {
        std::swap(vec[parent], vec[i]);
      } else {
        break;
      }
      i = parent;
    }
  }

  // Sink node down as necessary by swapping
  void sift_down(int i, int last_idx, std::vector<T>& vec) {
    int smallest_idx = i;

    int l = 2 * i + 1;
    int r = 2 * i + 2;

    // Check if left/right nodes exist and are smaller
    if ((l <= last_idx) && (vec[i] > vec[l])) {
      smallest_idx = l;
    }
    if ((r <= last_idx) && (vec[smallest_idx] > vec[r])) {
      smallest_idx = r;
    }

    if (smallest_idx != i) {
      std::swap(vec[i], vec[smallest_idx]);
      sift_down(smallest_idx, last_idx, vec);
    }
  }
};

#endif
