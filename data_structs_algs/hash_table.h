#include <exception>
#include <functional>
#include <iostream>
#include <utility>
#include <vector>

// Typically this is implemented with linked list?

template <typename K, typename T>
class HashTable {
  typedef std::pair<K, T> KeyValuePair;

 public:
  HashTable(int size) : size_(size), table_(size){};

  // Adds or modifies for key
  void add(K key, T value) {
    int h = hash(key);
    int existing_idx = find_key_pos(key);

    if (existing_idx != -1) {  // if it exists already
      table_[h][existing_idx].second = value;
    } else {
      table_[h].push_back(std::make_pair(key, value));
    }
  }

  bool exists(K key) {
    if (find_key_pos(key) != -1) {
      return true;
    } else {
      return false;
    }
  }

  T get(K key) {
    int existing_idx = find_key_pos(key);
    if (existing_idx != -1) {
      return table_[hash(key)][existing_idx].second;
    } else {
      throw std::runtime_error("Key doesn't exist");
    }
  }

  void remove(K key) {
    int existing_idx = find_key_pos(key);
    if (existing_idx != -1) {
      std::vector<KeyValuePair>& bin = table_[hash(key)];
      bin.erase(bin.begin() + existing_idx);
    } else {
      throw std::runtime_error("Key doesn't exist");
    }
  }

 private:
  int size_;
  std::vector<std::vector<KeyValuePair>> table_;
  std::hash<K> hash_func;

  int hash(K key) { return hash_func(key) % size_; }

  // TODO make this better
  // returns index to matching key, or -1
  int find_key_pos(K key) {
    int h = hash(key);
    std::vector<KeyValuePair>& bin = table_[h];
    for (int i = 0; i < bin.size(); ++i) {
      if (bin[i].first == key) {
        return i;
      }
    }
    return -1;
  }
};
