### To-do

* Trajectory optimization
  * melliner minimum-snap
  * richter
  * LCTO
  * hkust b-spline methods

* MPL methods
  * LQMT
  * other ways to generate trajectories, do MPL planning
    * BIAS

* path-planning
  * JPS
    * read the paper one more time to make sure I understand it
    * anything special about 3d?
    * figure out cleaner logic for pruning
  * additional methods that use distance field (understand the distance field algorithm too)
    * fast marching method
    * elastic band optimization
  * hkust b-spline kinodynamic path-palnning

* fix my python environment
  * setting up a python environment correctly
  * setting up python virtualenv https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/
  * packages
  * autoformatting python
* clean up
  * general refactoring
  * handle start==goal, no path found
  * the object-oriented way of using matplotlib
  * proper doxy
* figure out best workflow for making maps without matlab
* python optimization (jit, others)

### fast marching method

* Very similar to ROS cost potential search -> gradient descent. The only difference is that instead of ROS cost, the map contains information on "max speed", which increases as distance from obstacles increases. Because this "cost" has a physical meaning, we can use this as a path-parametrization.
  * This is a solution to a PDE; thus, the "cost-to-go" for each cell is updated with information from multiple neighboring cells
  * But this doesn't take into account current velocity. What if we made the "slowness map" represent acceleration instead?
* Eikonal equation = PDE representing wave propagation
  * https://en.wikipedia.org/wiki/Eikonal_equation#Numerical_approximation
* https://en.wikipedia.org/wiki/Fast_marching_method
* https://e-archivo.uc3m.es/bitstream/handle/10016/20321/path_RAM_2013_ps.pdf
* comparison vs dijkstra: http://www.ccgalberta.com/ccgresources/report12/2010-113_fmm_for_shortest_distance.pdf
  * dijkstra is faster, but also uses more memory (more pre-allocated data structures?)