#!/usr/bin/python3

import numpy as np

try:
  from .planner_helpers import PriorityQ, UNDEFINED, reconstruct_path, get_expanded_nodes
  from .map_helpers import *
except Exception: # workaround...
  from planners.planner_helpers import PriorityQ, UNDEFINED, reconstruct_path, get_expanded_nodes
  from planners.map_helpers import *

"""
occ_map: 2d numpy array. 0=free, 255=occ
start: 2x1 nparray
goal: 2x1 nparray

returns: path as list of 2d nparrays
"""
def dijkstra(occ_map, start, goal, verbose=False):
  if verbose:
    print('Running Dijkstra')
  shape = occ_map.shape
  width, height = shape

  # TODO put more assertions
  assert(is_in_bounds(start, shape))
  assert(is_in_bounds(goal, shape))

  source = xy_to_idx(start[0], start[1], shape)
  sink = xy_to_idx(goal[0], goal[1], shape)
  num_cells = width * height

  # initialize data structures
  dist = np.full(num_cells, float("inf"))
  prev = np.full(num_cells, UNDEFINED)
  opened = set()
  q = PriorityQ() # pass in tuples of (cost, cell_idx)

  dist[source] = 0.0
  q.push([0.0, source])

  while not q.empty():
    _, u = q.pop()
    opened.add(u)

    if u == sink:
      if verbose:
        print("Found goal! Number of opened nodes: ",  len(opened))
      break

    for v in indirect_neighbors(u, shape):
      if v in opened:
        continue;

      x, y = idx_to_xy(v, shape)
      v_cost = float("inf") if occ_map[x, y] == OCCUPIED else 0.0
      alt = dist[u] + v_cost + euclidean_distance(u, v, shape)
      if alt < dist[v]:
        dist[v] = alt
        prev[v] = u
        q.push([alt, v])

  return reconstruct_path(source, sink, prev, shape), get_expanded_nodes(prev)

if __name__ == "__main__":
  import pdb;pdb.set_trace()