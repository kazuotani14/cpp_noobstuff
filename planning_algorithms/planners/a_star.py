#!/usr/bin/python3

import numpy as np

try: # workaround for IDE
  from .planner_helpers import PriorityQ, UNDEFINED, reconstruct_path, get_expanded_nodes
  from .map_helpers import *
except Exception: # workaround...
  from planners.planner_helpers import PriorityQ, UNDEFINED, reconstruct_path, get_expanded_nodes
  from planners.map_helpers import *


"""
occ_map: 2d numpy array. 0=free, 255=occ
start: 2x1 nparray
goal: 2x1 nparray

returns: path as list of 2d nparrays
"""
def a_star(occ_map, start, goal, verbose=False):
  if verbose:
    print('Running A*')

  shape = occ_map.shape
  width, height = shape

  # TODO put more assertions
  assert(is_in_bounds(start, shape))
  assert(is_in_bounds(goal, shape))

  num_cells = width * height
  source = xy_to_idx(start[0], start[1], shape)
  sink = xy_to_idx(goal[0], goal[1], shape)

  # initialize data structures
  g_scores = np.full(num_cells, float("inf"))
  g_scores[source] = 0.0

  prev = np.full(num_cells, UNDEFINED)

  q = PriorityQ() # pass in tuples of (f_score, cell_idx)
  q.push(([0.0, source]))

  while not q.empty():
    _, u = q.pop()

    if u == sink:
      if verbose:
        print("Found goal! Number of opened nodes: ",  prev[prev != UNDEFINED].size)
      break

    for v in indirect_neighbors(u, shape):
      x, y = idx_to_xy(v, shape)
      v_cost = float("inf") if occ_map[x, y] == OCCUPIED else 0.0
      g_score = g_scores[u] + v_cost + euclidean_distance(u, v, shape)
      if g_score < g_scores[v]:
        prev[v] = u
        g_scores[v] = g_score
        h_score = g_score + euclidean_distance(v, sink, shape)
        q.push([h_score, v])

  return reconstruct_path(source, sink, prev, shape), get_expanded_nodes(prev)

if __name__ == "__main__":
  import pdb;pdb.set_trace()