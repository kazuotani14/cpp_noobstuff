#!/usr/bin/python3

import math

FREE = 0
OCCUPIED = 255

# All of these helpers are slow, but as long as all planners use the same helpers we can get relative speed comparisons

"x-major"
def xy_to_idx(x, y, shape):
  return x + y * shape[0]

def idx_to_xy(idx, shape):
  y = math.floor(idx / shape[0])
  x = idx - (shape[0] * y)
  return (x, y)

"Return indices of 4-connected neighbors"
def direct_neighbors(idx, shape):
  x, y = idx_to_xy(idx, shape)
  neighbors = []
  if x < shape[0] - 1:
    neighbors.append(xy_to_idx(x + 1, y, shape))
  if x > 0:
    neighbors.append(xy_to_idx(x - 1, y, shape))
  if y < shape[1] - 1:
    neighbors.append(xy_to_idx(x, y + 1, shape))
  if y > 0:
    neighbors.append(xy_to_idx(x, y - 1, shape))
  return neighbors

"Return indices of 8-connected neighbors"
def indirect_neighbors(idx, shape):
  x, y = idx_to_xy(idx, shape)
  neighbors = direct_neighbors(idx, shape)
  if x < shape[0] - 1 and y < shape[1] - 1:
    neighbors.append(xy_to_idx(x + 1, y + 1, shape))
  if x > 0 and y > 0:
    neighbors.append(xy_to_idx(x - 1, y - 1, shape))
  if x < shape[0] - 1 and y > 0:
    neighbors.append(xy_to_idx(x + 1, y - 1, shape))
  if x > 0 and y < shape[0] - 1:
    neighbors.append(xy_to_idx(x - 1, y + 1, shape))
  return neighbors

def euclidean_distance(idx1, idx2, shape):
  x1, y1 = idx_to_xy(idx1, shape)
  x2, y2 = idx_to_xy(idx2, shape)
  return math.sqrt((x1 - x2)**2 + (y1 - y2)**2)

def is_in_bounds(xy, shape):
  x = xy[0]
  y = xy[1]
  return x < shape[0] and x >= 0 and y < shape[1] and y >= 0

if __name__ == "__main__":
  shape = (5, 8)
  import pdb; pdb.set_trace()