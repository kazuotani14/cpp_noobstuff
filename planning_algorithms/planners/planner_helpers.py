#!/usr/bin/python3

import heapq
from .map_helpers import idx_to_xy

"""
Priority queue using heapq and list. Allows (expensive) priority updates
Pops off lowest costs first
Expects elements in list format [cost, label]
"""
class PriorityQ:
  def __init__(self):
    self.data = []

  def push(self, element):
    heapq.heappush(self.data, element)

  def pop(self):
    return heapq.heappop(self.data)

  def empty(self):
    return len(self.data) == 0

  def modify_priority(self, label, new_cost):
    # TODO do this more efficiently
    for element in self.data:
      if element[1] == label:
        element[0] = new_cost
        heapq.heapify(self.data)
        break

UNDEFINED = 9999999

def reconstruct_path(start_idx, goal_idx, prev, shape):
  path = []
  cur_cell = goal_idx
  while prev[cur_cell] != UNDEFINED:
    path.insert(0, idx_to_xy(cur_cell, shape))
    cur_cell = prev[cur_cell]

  if cur_cell != start_idx:
    print("Failed to find path")
    return []
  return path

def get_expanded_nodes(prev):
  expanded = prev
  expanded[expanded != UNDEFINED] = 1
  expanded[expanded == UNDEFINED] = 0
  return expanded