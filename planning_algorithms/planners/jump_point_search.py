#!/usr/bin/python3

import numpy as np

try: # workaround for IDE
  from .planner_helpers import PriorityQ, UNDEFINED, reconstruct_path, get_expanded_nodes
  from .map_helpers import *
except Exception: # workaround...
  from planners.planner_helpers import PriorityQ, UNDEFINED, reconstruct_path, get_expanded_nodes
  from planners.map_helpers import *

def clamp1(value):
  return max(-1, min(+1, value))

"from -> to"
def direction(x, n):
  x_dir = clamp1(n[0] - x[0])
  y_dir = clamp1(n[1] - x[1])
  return [x_dir, y_dir]

"return list of [x, y], without considering obstalces (forced neighbors). i.e. this returns natural neighbors _ "
def get_natural_neighbors(parent, x):
  x_dir, y_dir = direction(parent, x)
  horizontal = bool(x_dir) != bool(y_dir) #xor
  diagonal = bool(x_dir) and bool(y_dir)

  pruned_neighbors = []
  if horizontal:
    pruned_neighbors.append([x[0] + x_dir, x[1] + y_dir])
  elif diagonal:
    pruned_neighbors.append([x[0] + x_dir, x[1]])
    pruned_neighbors.append([x[0], x[1] + y_dir])
    pruned_neighbors.append([x[0] + x_dir, x[1] + y_dir])
  return pruned_neighbors

"x, direction taken to x, map"
def get_forced_neighbors(x, direction, occ_map):
  x_dir = direction[0]
  y_dir = direction[1]
  horizontal = bool(x_dir) != bool(y_dir) #xor
  diagonal = bool(x_dir) and bool(y_dir)

  forced_neighbors = []
  if horizontal and x_dir:
    for dy in [-1, 1]:
      cell_to_check = [x[0], x[1] + dy]
      if is_in_bounds(cell_to_check, occ_map.shape) and occ_map[cell_to_check[0], cell_to_check[1]] == OCCUPIED:
        forced_neighbors.append([cell_to_check[0] + x_dir, cell_to_check[1]])
  elif horizontal and y_dir:
    for dx in [-1, 1]:
      cell_to_check = [x[0] + dx, x[1]]
      if is_in_bounds(cell_to_check, occ_map.shape) and occ_map[cell_to_check[0], cell_to_check[1]] == OCCUPIED:
        forced_neighbors.append([cell_to_check[0], cell_to_check[1] + y_dir])
  elif diagonal:
    cell_to_check = [x[0] - x_dir, x[1]]
    if is_in_bounds(cell_to_check, occ_map.shape) and occ_map[cell_to_check[0], cell_to_check[1]] == OCCUPIED:
      forced_neighbors.append([cell_to_check[0], cell_to_check[1] + y_dir])
    cell_to_check = [x[0], x[1] - y_dir]
    if is_in_bounds(cell_to_check, occ_map.shape) and occ_map[cell_to_check[0], cell_to_check[1]] == OCCUPIED:
      forced_neighbors.append([cell_to_check[0] + x_dir, cell_to_check[1]])
  else:
    print('something went wrong')
  return forced_neighbors

def jump(x, direction, start, goal, occ_map):
  # TODO figure out when start is used
  n = [x[0] + direction[0], x[1] + direction[1]]

  if not is_in_bounds(n, occ_map.shape) or occ_map[n[0], n[1]] == OCCUPIED:
    return "null"

  if n[0]==goal[0] and n[1]==goal[1]:
    return n

  # check for forced neighbors
  if len(get_forced_neighbors(n, direction, occ_map)) > 0:
    return n

  # if diagonal, check in both grid directions
  if bool(direction[0]) and bool(direction[1]):
    for i in range(2):
      new_direction = [0, 0]
      new_direction[i] = direction[i]
      if jump(n, new_direction, start, goal, occ_map) != "null":
        return n

  return jump(n, direction, start, goal, occ_map)

def identify_successors(x, parent, occ_map, start, goal):
  successors = []
  shape = occ_map.shape

  if parent == UNDEFINED:
    neighbors = [idx_to_xy(idx, shape) for idx in indirect_neighbors(xy_to_idx(x[0], x[1], shape), shape)]
  else:
    neighbors = get_natural_neighbors(parent, x)
    neighbors = neighbors + get_forced_neighbors(x, direction(parent, x), occ_map)

    # hacky method for removing duplicates
    neighbors_2 = []
    neighbors = [neighbors_2.append(x) for x in neighbors if x not in neighbors_2]
    neighbors = neighbors_2

  for neighbor in neighbors:
    jump_cell = jump(x, direction(x, neighbor), start, goal, occ_map)
    if jump_cell is not "null":
      successors.append(jump_cell)
  return successors

"""
occ_map: 2d numpy array. 0=free, 255=occ
start: 2x1 nparray
goal: 2x1 nparray

returns: path as list of 2d nparrays
"""
def jump_point_search(occ_map, start, goal, verbose=False):
  if verbose:
    print('Running JPS')

  shape = occ_map.shape
  width, height = shape
  assert(is_in_bounds(start, shape))
  assert(is_in_bounds(goal, shape))

  num_cells = width * height
  source = xy_to_idx(start[0], start[1], shape)
  sink = xy_to_idx(goal[0], goal[1], shape)

  # initialize data structures
  dist = np.full(num_cells, float("inf"))
  prev = np.full(num_cells, UNDEFINED)
  q = PriorityQ() # pass in tuples of (cost, cell_idx)

  q.push([0.0, source])
  dist[source] = 0.0

  while not q.empty():
    _, u = q.pop()

    if u == sink:
      if verbose:
        print("Found goal! Number of opened nodes: ",  prev[prev != UNDEFINED].size)
      break

    parent = UNDEFINED if prev[u] == UNDEFINED else idx_to_xy(prev[u], shape)
    for x, y in identify_successors(idx_to_xy(u, shape), parent, occ_map, start, goal):
      v = xy_to_idx(x, y, shape)
      v_cost = float("inf") if occ_map[x, y] == OCCUPIED else 0.0
      g_score = dist[u] + v_cost + euclidean_distance(u, v, shape)
      if g_score < dist[v]:
        dist[v] = g_score
        prev[v] = u
        h_score = g_score + euclidean_distance(v, sink, shape)
        q.push([h_score, v])

  return reconstruct_path(source, sink, prev, shape), get_expanded_nodes(prev)

if __name__ == "__main__":
  # test get_natural_neighbors -- find natural neighbors
  shape = (5, 8)
  x = (1, 1)
  parent = (2, 2)
  u = xy_to_idx(x[0], x[1], shape)

  indirect_n = [idx_to_xy(idx, shape) for idx in indirect_neighbors(u, shape)];
  neighbors = get_natural_neighbors(parent, x)
  # print(neighbors)

  # test has_forced_neighbors
  occ_map_simple = np.zeros((3, 3))
  occ_map_simple[1, 0] = OCCUPIED
  x = [1, 1]
  # print(has_forced_neighbors(x, [1, 0], occ_map_simple))
  # print(has_forced_neighbors(x, [0, -1], occ_map_simple))
  # print(has_forced_neighbors(x, [1, 1], occ_map_simple))
  # print(has_forced_neighbors(x, [1, -1], occ_map_simple))

  # test jump
  """
  _ _ _ g _
  _ _ _ _ _
  _ _ _ _ _
  _ s _ _ _
  _ _ _ _ _

  x ->, ^y
  s=start, g=goal, _=free, o=obstacle
  """
  occ_map_simple_2 = np.zeros((5, 5))
  x = [1, 1]
  start = [] # not used???
  goal = [3, 4]
  # print(jump(x, [1, 1], [], goal, occ_map_simple_2)) # expect (3, 3)
  # print(jump(x, [-1, -1], [], goal, occ_map_simple_2)) # expect null
  # print(jump(x, [1, 0], [], goal, occ_map_simple_2)) # expect null
  # print(jump(x, [0, 1], [], goal, occ_map_simple_2)) # expect null
  # print(jump(x, [0, -1], [], goal, occ_map_simple_2)) # expect null
  # print(jump(x, [-1, 0], [], goal, occ_map_simple_2)) # expect null
  # print(jump(x, [-1, 1], [], goal, occ_map_simple_2)) # expect null
  # print(jump(x, [1, -1], [], goal, occ_map_simple_2)) # expect null

  # print(identify_successors(x, UNDEFINED, occ_map_simple_2, start, goal)) # (3, 3)

  """
  _ _ _ g _
  _ _ _ _ _
  _ _ o _ _
  s _ _ _ _
  _ _ _ _ _
  """
  occ_map_simple_3 = np.zeros((5, 5))
  occ_map_simple_3[2, 2] = OCCUPIED
  x = [0, 1]
  start = [] # not used???
  goal = [3, 4]
  # print(jump(x, [1, 0], [], goal, occ_map_simple_3)) # expect (2, 1)
  # print(jump(x, [1, 1], [], goal, occ_map_simple_3)) # expect (2, 3)

  # print(identify_successors(x, UNDEFINED, occ_map_simple_3, start, goal)) #expect both from above
  # print(identify_successors([2, 3], x, occ_map_simple_3, start, goal)) #expect one forced neighbor

  """
  _ _ _ _ g
  _ _ _ _ _
  o o o o _
  _ _ _ _ _
  s _ _ _ _
  """
  occ_map_simple_4 = np.zeros((5, 5))
  occ_map_simple_4[0:4, 2] = OCCUPIED
  start = [0, 0]
  goal = [4, 4]
  # print(identify_successors(start, UNDEFINED, occ_map_simple_3, start, goal)) #expect (1, 1)
  # print(identify_successors([1,1], start, occ_map_simple_4, start, goal)) # expect (2, 1)
  # print(identify_successors([2,1], [1,1], occ_map_simple_4, start, goal)) # expect (3, 1)
  print(identify_successors([3,1], [2,1], occ_map_simple_4, start, goal)) # expect (4, 2)
  # print('---')
  # print(jump([3,1], [1,0], start, goal, occ_map_simple_4))

  # print(get_forced_neighbors([3,1], [1,0], occ_map_simple_4))

  # print(occ_map_simple_4)



  # import matplotlib.image as mpimg
  # map_path = "maps/middle_obstacle.png"
  # print('Loading map from {}'.format(map_path))
  # occ_map = mpimg.imread(map_path).astype(int) * 255

  # identify_successors(u, occ_map)