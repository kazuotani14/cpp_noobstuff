#ifndef _DIJKSTRA_H_
#define _DIJKSTRA_H_

#include <iostream>
#include <queue>
#include <vector>
#include <functional>
#include <eigen/Eigen/Core>

using Eigen::Vector2d;

class EnvironmentObstacle {

private:


}

// Defines the problem for the planner
class Environment2D {
public:
 Environment2D();


 const Vector2d& start() { return start_; };
 const Vector2d& goal() { return goal_; };

private:
 Vector2d start_;
 Vector2d goal_;


};

class DijkstraPlanner {
public:
 DijkstraPlanner();


private:
 std::priority_queue<double, Vector2d, std::greater<double>> Q; //push, top, pop (top), empty, size

};

#endif
