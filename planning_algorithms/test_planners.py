#!/usr/bin/python3

import copy
import math
import sys
import time

import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np

from planners.dijkstra import dijkstra
from planners.a_star import a_star
from planners.jump_point_search import jump_point_search

from planners.map_helpers import FREE, OCCUPIED, idx_to_xy

"""
Euclidean distance transform from http://www.logarithmic.net/pfh/blog/01185880752
"""
def _upscan(f):
    for i, fi in enumerate(f):
        if fi == np.inf: continue
        for j in range(1,i+1):
            x = fi+j*j
            if f[i-j] < x: break
            f[i-j] = x

def distance_transform(bitmap):
    f = np.where(bitmap, 0.0, np.inf)
    for i in range(f.shape[0]):
        _upscan(f[i,:])
        _upscan(f[i,::-1])
    for i in range(f.shape[1]):
        _upscan(f[:,i])
        _upscan(f[::-1,i]) # [::-1] is reverse
    np.sqrt(f,f)
    return f


if __name__ == "__main__":
  # Parse arguments
  plot_path = False # TODO figure this out. use an argument parser
  map_name = "zigzag" if len(sys.argv) <= 2 else sys.argv[2]

  # Load map
  map_path = "maps/" + map_name + ".png"
  print('Loading map from {}'.format(map_path))
  occ_map = mpimg.imread(map_path).astype(int) * 255
  height, width = occ_map.shape

  # Define start and goal points
  start = [0, 0] if len(sys.argv) <= 4 else [sys.argv[3], sys.argv[4]]
  # goal = [height - 1, width - 1] if len(sys.argv) <= 6 else [sys.argv[5], sys.argv[6]]
  goal = [50, 90]

  print('map dims: ', occ_map.shape)
  print('start: ', start)
  print('goal: ', goal)

  print('Planning...')
  t_start = time.time()

  # path, expanded = dijkstra(occ_map, start, goal, verbose=True)
  # path, expanded = a_star(occ_map, start, goal, verbose=True)
  path, expanded = jump_point_search(occ_map, start, goal, verbose=True)

  t_done = time.time()
  print('Took {:.3f} seconds'.format(t_done - t_start))
  # print(path)

  # Plot path
  if plot_path:
    RED = (255, 0, 0)
    GREEN = (0, 255, 0)
    BLUE = (0, 0, 255)
    GRAY = (200, 200, 200)
    occ_map_inv = 255 - occ_map # invert for plotting
    occ_map_with_path = np.stack([occ_map_inv, occ_map_inv, occ_map_inv], axis=2)

    for i in range(expanded.size):
      if expanded[i] > 0:
        x, y = idx_to_xy(i, occ_map.shape)
        if occ_map[x,y] != OCCUPIED:
          occ_map_with_path[x,y] = GRAY

    for point in path:
      occ_map_with_path[point[0], point[1]] = BLUE

    occ_map_with_path[start[0], start[1]] = RED
    occ_map_with_path[goal[0], goal[1]] = GREEN

    plt.imshow(occ_map_with_path)
    plt.show()

  # Profile
  def profile_planner(name, plan_func, occ_map, start, goal):
    t_start = time.time()
    for i in range(10):
      path, expanded = plan_func(occ_map, start, goal)
    t_done = time.time()
    print('{} took {:.3f} seconds'.format(name, t_done - t_start))

  print("Profiling over 10 runs...")
  profile_planner("Dijkstra", dijkstra, occ_map, start, goal)
  profile_planner("A*", a_star, occ_map, start, goal)
  profile_planner("JPS", jump_point_search, occ_map, start, goal)

  # Compute distance transform (0 at obstacles, distance in cells from nearest obstacle for free cells)
  occ_map_binary = occ_map
  occ_map_binary[occ_map_binary != 0] = 1
  dist_field = distance_transform(occ_map_binary)
  # plt.imshow(dist_field)
  # plt.show()