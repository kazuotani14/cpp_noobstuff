#include <algorithm>
#include <functional>
#include <iostream>
#include <queue>
#include <stack>
#include <string>
#include <utility>  // pair
#include <vector>
// TODO add algorithm (sort), lower_bound

#include <algorithm>  // find
#include <limits>     //numeric_limits::infinity(0)
#include <queue>      // push, front, pop, empty
#include <stack>      // push, top, pop, empty

using namespace std;

int main(int argc, char* argv[]) {
  vector<int> v{1, 2, 3, 4, 5};

  auto find_idx = std::find(v.begin(), v.end(), 2);
  if (find_idx != v.end()) 
    cout << *find_idx << " is in v at index " << find_idx - v.begin() << endl;

  v.push_back(2);
  int n = count(v.begin(), v.end(), 2);
  cout << "number of 2s: " <<  n << endl;

  // generic lambda
  auto print_container = [](auto cont) { for(const auto e:cont) cout << e << " "; cout << "\n"; };

  // To remove element by value: remove returns iterator
  print_container(v);
  v.erase(std::remove(v.begin(), v.end(), 4), v.end());
  print_container(v);

  // Priority queue is max by default - use std::greater
  using IntStrPair = pair<int, string>;
  priority_queue<IntStrPair, std::vector<IntStrPair>, greater<IntStrPair>> pq;
  pq.push(make_pair(1, "one"));
  pq.push(make_pair(2, "one"));
  pq.push(make_pair(0, "one"));
  cout << pq.top().first << endl;
  pq.pop();
  cout << pq.top().first << endl;

  return 0;
}
