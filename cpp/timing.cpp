#include <chrono>
#include <string>
#include <thread>

void f(std::string s1, std::string s2) {
  // These using's allow the use of `1000ms` in place of
  // `std::chrono::milliseconds(1000)` C++14 duration suffixes
  using namespace std::literals;

  std::this_thread::sleep_for(1000ms);
  std::cout << s1 << " " << s2 << std::endl;
}

int main() {
  // start's type: std::chrono::time_point<std::chrono::steady_clock>
  // steady_clock is better for measuring duration, system_clock is for absolute
  // time (e.g. seconds after epoch)

  using namespace std::chrono;
  auto start = steady_clock::now();
  auto now = steady_clock::now();
  long int elapsed = duration_cast<milliseconds>(now - start).count();
  std::cout << "Milliseconds elapsed: " << elapsed << std::endl;

  // Alternative method: use C++14 forwarding and universal refereces with
  // lambda to wrap function call
  // Args... denotes a parameter pack (zero or more inputs)

  auto timeFuncCall = [](auto&& func, auto&&... params) {
    using namespace std::chrono;

    auto start = steady_clock::now();
    std::forward<decltype(func)>(func)(
        std::forward<decltype(params)>(params)...);
    auto now = steady_clock::now();
    long int elapsed = duration_cast<milliseconds>(now - start).count();
    std::cout << "Milliseconds elapsed: " << elapsed << std::endl;
  };

  timeFuncCall(f, "this", "works!");
}
