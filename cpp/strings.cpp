#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
  string test =
      "what "
      "this works?";
  cout << "what "
               "this works?"
            << endl;
  // This works because the two char[] values are just concatenated before std::string construction

  string s = "hello!";
  cout << s << " : " << (s.compare(0, 1, "h")==0) << endl;

  string num1 = "10";
  string num2 = to_string(5);
  cout << stoi(num1) + stoi(num2) << endl;

  return 0;
}
