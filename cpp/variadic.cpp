/*

Variadic templates allow functions that can take an arbitrary number of
arguments and operate on them recursively.

*/

#include <iostream>
#include <vector>

// Variadic template function that does nothing
template <typename... Args>
void ignore(Args... args) {}

// Base case
template <typename T>
double sum(T t) {
  return t;
}

// Recursive sum
template <typename T, typename... Rest>
double sum(T t, Rest... rest) {
  return t +
         sum(rest...);  // the '...' in 'rest...' unpacks rest - e.g. sum(2,3)
}

int main(int argc, char *argv[]) {
  ignore();
  ignore(1, 2.0, "true");

  std::cout << sum(1, 2, 3) << std::endl;

  return 0;
}
