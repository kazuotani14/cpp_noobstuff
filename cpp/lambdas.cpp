/*
    Lambdas are useful for making succint operations with the standard library,
   and defining small functions. [capture list](parameter list) ->
   returntype{function body} Return type is optional, but can make things more
   clear

    Return type is assume to be void if no 'return', or of the type of single
   return statement
*/

#include <algorithm>
#include <iostream>
#include <vector>

int main(int argc, char *argv[]) {
  std::vector<int> int_vec = {1, 2, 3, 4, 5};
  for_each(int_vec.begin(), int_vec.end(),
           [](int i) { std::cout << ":" << i << ":"; });
  std::cout << "\n";

  auto great_than_3_count = [](std::vector<int> v) -> int {
    return count_if(v.begin(), v.end(), [](int x) { return (x > 3); });
  };
  std::cout << "Number of elements greater than 3: "
            << great_than_3_count(int_vec) << std::endl;

  // Lambda expression creates a function object
  auto f = [](int a, int b) -> int { return a + b; };
  std::cout << f(1, 2) << std::endl;

  // Lambdas can also be used to "capture" variables that were in local block
  // scope when it was created (copied as member variables in function object)
  int c1 = 10;
  double c2 = 0.5;
  auto g = [c1, c2]() { std::cout << c1 + c2 << std::endl; };
  g();

  // Can also capture by reference
  // Individual variables, or:
  // all variables in local block scope with copy [=]
  // all variables in local block scope with reference [&]
  // To access member variables with lambda defined inside class, use [this]
  auto g2 = [&c1, &c2]() { std::cout << c1 + c2 << std::endl; };
  for (auto i : {1, 2, 3}) {
    c1 = i;
    g2();
  }
}
