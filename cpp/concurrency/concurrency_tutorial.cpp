/*
Based on: https://baptiste-wicht.com/posts/2012/03/cpp11-concurrency-part1-start-threads.html
*/

#include <thread>
#include <iostream>
#include <vector>
#include <mutex>

// Replace with lambda
// void hello() {
// 	std::cout << "Hello from thread " << std::this_thread::get_id() << std::endl;
// }

int main() {
	std::vector<std::thread> threads;
	std::mutex mutex;

	for(int i=0; i<5; ++i) {
		threads.push_back(std::thread([&mutex](){
			std::lock_guard<std::mutex> lock(mutex); // removing will lead to weird print
			std::cout << "Hello from thread " << std::this_thread::get_id() << std::endl;
		}));
	}

	for (auto & thread: threads){
		thread.join();
	}

	return 0;
}
