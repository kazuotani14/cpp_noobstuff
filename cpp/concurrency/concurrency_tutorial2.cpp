/*
Based on: https://baptiste-wicht.com/posts/2012/03/cp11-concurrency-tutorial-part-2-protect-shared-data.html
*/

#include <thread>
#include <mutex>
#include <iostream>
#include <vector>

struct Counter {
	int value;

	Counter(): value(0){}

	void increment(){
		++value;
	}

	void decrement(){
		if(value==0){
			throw "Value cannot be less than 0";
		}

		--value;
	}
};

struct ConcurrentCounter {
	std::mutex mutex;
	Counter counter;

	void increment(){
		// Two ways to lock: with lock_guard, or lock method
		std::lock_guard<std::mutex> lock(mutex);
		counter.increment();
	}

	void decrement() {
		std::lock_guard<std::mutex> lock(mutex);
		counter.decrement();
	}
};

int main(){
	ConcurrentCounter counter;

	std::vector<std::thread> threads;
	for(int i=0; i<5; ++i){
		threads.push_back(std::thread([&counter](){
			for(int i=0; i<100; ++i){
				counter.increment();
			}
		}));
	}

	for(auto& thread : threads){
		thread.join();
	}

	std::cout << counter.counter.value << std::endl;

	return 0;
}
