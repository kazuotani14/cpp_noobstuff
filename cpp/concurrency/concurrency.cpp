/*
Prefer task-based concurrency to thread-based concurrency (ie std::async over
std::thread), because this offloads thread management to the standard library
This means that you'll never get an out-of-threads error (OS can't have any more
software threads)

To set thread priorities, you'll need to access the lower-level
platform-specific API, which will probably be pthreads
*/

#include <future>
#include <iostream>
#include <thread>

int f() {
  std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  return 1;
}

int main(int argc, char *argv[]) {
  auto fut = std::async(f);
  std::cout << fut.get() << std::endl;
  return 0;
}
