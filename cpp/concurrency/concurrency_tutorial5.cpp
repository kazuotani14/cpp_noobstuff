/*
Based on: https://baptiste-wicht.com/posts/2012/04/c11-concurrency-tutorial-advanced-locking-and-condition-variables.html
*/

#include <thread>
#include <mutex>
#include <iostream>
#include <vector>

std::once_flag flag;

void do_something(){
	std::call_once(flag, [](){std::cout << "Called once" << std::endl;});

	std::cout << "Called each time" << std::endl;
}

int main() {
	std::vector<std::thread> threads;

	for(int i=0; i<4; i++){
		threads.push_back(std::thread(do_something));
	}

	for(auto& thread : threads){
		thread.join();
	}

	return 0;
}
