/*
Based on: https://baptiste-wicht.com/posts/2012/04/c11-concurrency-tutorial-advanced-locking-and-condition-variables.html
*/

#include <thread>
#include <mutex>
#include <iostream>
#include <vector>

std::timed_mutex mutex;
int counter = 0;

void work(){
	std::chrono::milliseconds timeout(100);

	while(counter < 5){
		if(mutex.try_lock_for(timeout)){
			std::cout << std::this_thread::get_id() << ": do work with the mutex" << std::endl;

			std::chrono::milliseconds sleepDuration(250);
			std::this_thread::sleep_for(sleepDuration);
			counter++;

			mutex.unlock();

			std::this_thread::sleep_for(sleepDuration);
		}
		else {
			std::cout << std::this_thread::get_id() << ": do work without the mutex" << std::endl;
			std::chrono::milliseconds sleepDuration(100);
			std::this_thread::sleep_for(sleepDuration);
		}
	}
}

int main() {
	std::thread t1(work);
	std::thread t2(work);

	t1.join();
	t2.join();

	return 0;
}
