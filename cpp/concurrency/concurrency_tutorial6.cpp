/*
Based on: https://baptiste-wicht.com/posts/2012/04/c11-concurrency-tutorial-advanced-locking-and-condition-variables.html
*/

#include <thread>
#include <mutex>
#include <iostream>
#include <vector>

struct BoundedBuffer {
	int* buffer;
	int capacity;

	int front;
	int rear;
	int count;

	std::mutex lock;

	std::condition_variable not_full;
	std::condition_variable not_empty;
	std::chrono::seconds timeout;

	BoundedBuffer(int capacity) : capacity(capacity), front(0), rear(0), count(0), timeout(2) {
		buffer = new int[capacity];
	}

	~BoundedBuffer(){
		delete[] buffer;
	}

	void deposit(int data){
		std::unique_lock<std::mutex> l(lock);

		if(not_full.wait_for(l, timeout, [this](){return count != capacity;})){
			buffer[rear] = data;
			rear = (rear + 1) % capacity;
			++count;

			not_empty.notify_one();
		}
		else{
			std::cout << "Producer got bored." << std::endl;
		}
	}

	int fetch(){
		std::unique_lock<std::mutex> l(lock);

		if(not_empty.wait_for(l, timeout, [this](){return count != 0;}))
		{
			int result = buffer[front];
			front = (front + 1) % capacity;
			--count;

			not_full.notify_one();

			return result;
		}
		else{
			std::cout << "Consumer starved." << std::endl;
			return 9999;
		}
	}
};

std::mutex print_mutex;
int count = 0;

void consumer(int id, BoundedBuffer& buffer){
    for(int i = 0; i < 10; ++i){
		print_mutex.lock();
		std::cout << "Enter consumer " << std::this_thread::get_id() << std::endl;
		print_mutex.unlock();

        int value = buffer.fetch();

		if (value != 9999){
			print_mutex.lock();
			std::cout << "Consumer " << id << " fetched " << value << std::endl;
			print_mutex.unlock();
			std::this_thread::sleep_for(std::chrono::milliseconds(300));
		}
    }
}

void producer(int id, BoundedBuffer& buffer){
    for(int i = 0; i < 10; ++i){
		print_mutex.lock();
		std::cout << "Enter producer " << std::this_thread::get_id() << std::endl;
		int value = ++count;
		print_mutex.unlock();

        buffer.deposit(value);

		print_mutex.lock();
        std::cout << "Produced " << id << " produced " << i << std::endl;
		print_mutex.unlock();
        std::this_thread::sleep_for(std::chrono::milliseconds(150));
    }
}

int main(){
  BoundedBuffer buffer(10);

  std::thread p1(producer, 0, std::ref(buffer));
  std::thread p2(producer, 1, std::ref(buffer));
	std::thread c1(consumer, 0, std::ref(buffer));
	std::thread c2(consumer, 1, std::ref(buffer));
	std::thread c3(consumer, 2, std::ref(buffer));

    c1.join();
    c2.join();
    c3.join();
    p1.join();
    p2.join();

    return 0;
}
