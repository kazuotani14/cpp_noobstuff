/*
Based on: https://baptiste-wicht.com/posts/2012/04/c11-concurrency-tutorial-advanced-locking-and-condition-variables.html
*/

#include <thread>
#include <mutex>
#include <iostream>
#include <vector>

struct Complex {
	std::recursive_mutex mutex;
	int i;

	Complex() : i(0) {}

	void mul(int x){
		std::lock_guard<std::recursive_mutex> lock(mutex);
		i *= x;
	}
	void div(int x){
		std::lock_guard<std::recursive_mutex> lock(mutex);
		i /= x;
	}
	void both(int x, int y){
	    std::lock_guard<std::recursive_mutex> lock(mutex);
	    mul(x);
		//Deadlock with std::mutex!
	    div(y);
	}
};

int main() {
	Complex complex;
	complex.both(32, 33);

	return 0;
}
