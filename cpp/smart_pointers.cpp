#include <iostream>
#include <memory>

using namespace std;

struct Widget {
  ~Widget() { cout << "widget destructor" << endl; };
  int data;
};

int main(int argc, char* argv[]) {
  // Widget w{2};
  Widget* w = new Widget{2};
  {
    unique_ptr<Widget> w_ptr;
    w_ptr.reset(w);
    cout << w->data << endl;
  }
  cout << w->data << endl; // behavior here is undefined
  return 0;
}
