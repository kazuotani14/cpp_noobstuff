#include <iostream>
#include <eigen3/Eigen/Core>

Eigen::Vector2d v2;
Eigen::Vector3d v3;

// static const int DIM = 2;
static const int DIM = 3;

template <int N>
class Vector
{
 public:
  Vector(){};
};

template <int I, std::enable_if_t<I == 2>* = nullptr>
void foo()
{
  std::cout << "I (" << I << ") am 2." << std::endl;
}

template <int I, std::enable_if_t<(I == 3)>* = nullptr>
void foo()
{
  std::cout << "I (" << I << ") am 3." << std::endl;
}

template <typename EigenVec, std::enable_if_t<(EigenVec::RowsAtCompileTime == 3)>* = nullptr>
EigenVec foo()
{
  std::cout << "Returning a vector of size 3" << std::endl;
  return EigenVec();
}

int main()
{
  std::cout << Eigen::Vector2d::RowsAtCompileTime << std::endl;
  foo<2>();
  foo<3>();
  Eigen::Vector3d a = foo<Eigen::Vector3d>();
  return 0;
}