#ifndef _EIGEN_HELPERS_H_
#define _EIGEN_HELPERS_H_

#include <Eigen/Core>
#include <Eigen/Eigenvalues>

Eigen::IOFormat CleanFmt(3, 0, ", ", "\n", "", "");

void print_eigen(const std::string name, const Eigen::MatrixXd& mat) {
  if (mat.cols() == 1) {
    std::cout << name << ": " << mat.transpose().format(CleanFmt) << std::endl;
  } else {
    std::cout << name << ":\n" << mat.format(CleanFmt) << std::endl;
  }
}

auto print_eigen = [](const std::string name, const Eigen::MatrixXd& mat)
{
  Eigen::IOFormat CleanFmt(4, 0, ", ", ";\n", "", "");
  if(mat.cols() == 1) {
    std::cout << name << "=[ " << mat.transpose().format(CleanFmt) << " ];" << std::endl;
  }
  else {
    std::cout << name << "=[\n" << mat.format(CleanFmt) << "\n];" << std::endl;
  }
};

void print_dims(std::string name, const Eigen::MatrixXd& mat) {
  std::cout << "dim(" << name << "): " << mat.rows() << ", " << mat.cols()
            << std::endl;
}

void check_positive_definite(const Eigen::MatrixXd& mat) {
  Eigen::LLT<Eigen::MatrixXd> lltOfMat(
      mat);  // compute the Cholesky decomposition of A
  if (lltOfMat.info() == Eigen::NumericalIssue) {
    std::cout << "Matrix is NOT positive semidefinite!" << std::endl;
  } else {
    std::cout << "Matrix is positive semidefinite!" << std::endl;
  }
}

void print_eigvecs(const Eigen::MatrixXd& mat) {
  Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eigensolver(mat);
  if (eigensolver.info() != Eigen::Success) {
    std::cout << "Failed to compute eigenvectors." << std::endl;
    return;
  }
  std::cout << "The eigenvalues of matrix are:\n"
            << eigensolver.eigenvalues().format(CleanFmt) << std::endl;
  // cout << "Here's a matrix whose columns are eigenvectors of A \n"
  // 	 << "corresponding to these eigenvalues:\n"
  // 	 << eigensolver.eigenvectors() << endl;
}

#endif
