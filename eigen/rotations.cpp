// g++ rotations.cpp -std=c++14 -I /usr/include/eigen3 -o test

#include <eigen3/Eigen/Dense>
#include <iostream>

using namespace Eigen;

int main()
{
  double yaw = M_PI / 4;
  Eigen::Matrix3d mat = Eigen::AngleAxisd(yaw, Eigen::Vector3d::UnitZ()).toRotationMatrix();

  std::cout << mat.block(0, 0, 2, 2) << std::endl;
}