/*
  Testing linear system solvers with Eigen
  https://eigen.tuxfamily.org/dox/group__TutorialLinearAlgebra.html
 
  Specifically, I wanted to understand solving underdetermined systems, and
  tradeoffs between different methods.
 
  To use COD, clone latest version of eigen from:
  http://eigen.tuxfamily.org/index.php?title=Main_Page
  Older versions don't have it
  https://bitbucket.org/eigen/eigen/pull-requests/163/implement-complete-orthogonal/diff
 
  g++ linear_system_solvers.cpp -std=c++14 -I /usr/include/eigen3 -o test
 
  Takeaways:
  * Qr is much faster than svd (how much depends on matrix size) but doesn't
  provide min-norm solution
  * COD is typically faster than SVD, and also gives min-norm solution to
  underdetermined set of equations
  * normal equations are faster than SVD or COD and get min-norm solution, but
    only when A^T*A is full rank (invertible)
 
  * Why is finding the min-norm solution to an underdetermined system important?
    * It's a unique solution - repeatable.
    * Minimum norm gives us some degree of confidence that it's a reasonable
    solution. In the case of polynomial fitting, it gives us more confidence
    that the polynomial doesn't do anything crazy in between the boundary
    conditions
 
  * Why SVD finds min-norm solution:
https://math.stackexchange.com/questions/974193/why-does-svd-provide-the-least-squares-and-least-norm-solution-to-a-x-b
  * Helpful slides:
    https://see.stanford.edu/materials/lsoeldsee263/08-min-norm.pdf
    * Note that in "Least-norm solution via QR factorization", they assume A is
    full-rank. We don't get the least-norm solution via QR if not, as seen in
    tests below
 
Trials with last row set to zeros, to make A not full-rank
 
5x5, 100 trials each
-- qr --
microseconds/solve: 638108
avg norm: 2.38065e+15
-- colpiv qr --
microseconds/solve: 323799
avg norm: 6.88027e+15
-- jacobi svd --
microseconds/solve: 4425204
avg norm: 5.63318
-- bcd svd --
microseconds/solve: 9175417
avg norm: 5.63318
-- cod --
microseconds/solve: 510670
avg norm: 5.63318

TODO re-run 10x10 and 5x5
10x10, 100 trials each - close to what we would do for fitting 8th order polynomial
-- qr --
microseconds/solve: 272
avg norm: 1.27299e+16
-- colpiv qr --
microseconds/solve: 282
avg norm: 2.92622e+15
-- jacobi svd --
microseconds/solve: 822
avg norm: 1.71501
-- bcd svd --
microseconds/solve: 945
avg norm: 1.71501
-- cod --
microseconds/solve: 512
avg norm: 1.71501
 
50x50, 100 trials each
-- qr --
microseconds/solve: 6513
avg norm: 6.1736e+15
-- colpiv qr --
microseconds/solve: 3232
avg norm: 1.28898e+16
-- jacobi svd --
microseconds/solve: 40189
avg norm: 1.9602
-- bcd svd --
microseconds/solve 83420
avg norm: 1.9602
-- cod --
microseconds/solve: 5262
avg norm: 1.9602
 
100x100, 100 trials each
-- qr --
microseconds/solve: 28663
avg norm: 1.78827e+15
-- colpiv qr --
microseconds/solve: 22334
avg norm: 1.77457e+15
-- jacobi svd --
microseconds/solve: 312357
avg norm: 8.61207
-- bcd svd --
microseconds/solve: 400145
avg norm: 8.61207
-- cod --
microseconds/solve: 20539
avg norm: 8.61207
 
*/
 
#include <chrono>
#include <eigen3/Eigen/Dense>
#include <iostream>
 
using namespace Eigen;
using namespace std::chrono;
 
int main() {
  /* Underdetermined system, from Drake tests */
  // MatrixXd A(2, 3);
  // A << 3, 1, 0, 1, 0, 1;
  // VectorXd b(2);
  // b << 1, 2;
 
  /* underdetermined system if rows < cols, i.e. "fat" matrix */
  int rows = 10;
  int cols = rows * 10;
  srand((unsigned int)time(0));
  MatrixXd A = MatrixXd::Random(rows, cols);
  // A.row(0) = VectorXd::Zero(rows * 10); // make normal equation fail
  VectorXd b = VectorXd::Random(rows);
 
  time_point<system_clock> start, now;
  long int elapsed;
  auto start_clock = [&start]() { start = system_clock::now(); };
  auto stop_clock = [&start]() {
    auto now = system_clock::now();
    auto elapsed = duration_cast<microseconds>(now - start).count();
    std::cout << "microseconds elapsed: " << elapsed << std::endl;
  };
 
  std::cout << "-- QR --" << std::endl;
  start_clock();
  VectorXd sol_qr = A.householderQr().solve(b);
  stop_clock();
  std::cout << "norm: " << sol_qr.norm() << std::endl;
 
  std::cout << "-- colpivQR --" << std::endl;
  start_clock();
  VectorXd sol_colpivqr = A.colPivHouseholderQr().solve(b);
  stop_clock();
  std::cout << "norm: " << sol_colpivqr.norm() << std::endl;
 
  std::cout << "-- Jacobi SVD --" << std::endl;
  start_clock();
  VectorXd sol_jsvd = A.jacobiSvd(ComputeThinU | ComputeThinV).solve(b);
  stop_clock();
  std::cout << "norm: " << sol_jsvd.norm() << std::endl;
 
  std::cout << "-- Normal equation --" << std::endl;
  start_clock();
  VectorXd sol_normal = A.transpose() * (A * A.transpose()).inverse() * b;
  stop_clock();
  std::cout << "norm: " << sol_normal.norm() << std::endl;
 
  // std::cout << "-- BDC SVD --" << std::endl;
  // start_clock();
  // VectorXd sol_bsvd = A.bdcSvd(ComputeThinU | ComputeThinV).solve(b);
  // stop_clock();
  // std::cout << "norm: " << sol_bsvd.norm() << std::endl;
 
  // std::cout << "-- fullPivLu --" << std::endl;
  // start_clock();
  // auto sol_fullpivlu = A.fullPivLu().solve(b);
  // stop_clock();
  // std::cout << "norm: " << sol_fullpivlu.norm() << std::endl;
 
  // std::cout << "-- completeOrthogonalDecomposition --" << std::endl;
  // start_clock();
  // sol_cod = A.completeOrthogonalDecomposition().solve(b);
  // stop_clock();
  // std::cout << "norm: " << sol_cod.norm() << std::endl;
 
  // std::cout << "-- Manual QR with A^T --" << std::endl;
  // auto qr = A.transpose().fullPivHouseholderQr();
  // MatrixXd qr_Q = qr.matrixQ();
  // MatrixXd qr_R = qr_Q.transpose() * qr.matrixQR();
  // VectorXd sol_qr_manual = qr_Q.block(0, 0, rows, rows) * (qr_R.block(0, 0,
  // rows, rows).transpose().inverse() * b);
  // std::cout << "norm: " << sol_qr_manual.norm() << std::endl;
 
  return 0;
}