// Trying Eigen autodiff
// From:
// https://github.com/edrumwri/drake/blob/bbc944fec87f7dac13169c65c961db29906435fb/drake/doc/autodiff_intro/autodiff.pdf
// g++ autodiff.cpp -std=c++11 -I /usr/include/eigen3 -o test_autodiff

// Autodiff takes advantage of the fact that computers execute sequences of
// elementary functions and arithmetic operations by taking the derivative at
// each step and using the chain rule, we can find total derivative

#include <Eigen/Core>
#include <iostream>
#include <unsupported/Eigen/AutoDiff>

template <typename T>
T sin_T(const T& a) {
  using std::sin;
  return sin(a);
}

template <typename T>
T y(const T& t) {
  return t * t;
}

using std::cout;
using std::endl;

int main(int argc, char* argv[]) {
  typedef Eigen::Matrix<double, 1, 1> Vector1d;
  typedef Eigen::AutoDiffScalar<Eigen::Matrix<double, 1, 1>> AScalar;
  AScalar x = 3 * M_PI / 4.0;

  // set as independent variable (not a final or intermediate result)
  // Otherwise it =0, which is like saying the variable is constant
  // Basically, this sets dx/dx = 1
  x.derivatives()(0) = 1;

  cout << "sin(y(x)) = " << sin_T(y(x)).derivatives()
       << " for x = " << x.value() << endl;
  //  cout << "d/dx sin = cos(" << x.value() << "^2) * 2 * " << x.value() << " =
  //  " << sin_T(x).derivatives() << endl;
  return 0;
}
