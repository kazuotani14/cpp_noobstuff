#ifndef _ROS_HELPERS_H_
#define _ROS_HELPERS_H_

#include <visualization_msgs/Marker.h>
#include <Eigen/Core>
#include <string>

// Ros param getter to save space and repetition. Assumes:
// * node has node handle pointer named nh_
// * name is a string
// * variable is a member variable name
#define GETROSPARAM(name, variable)            \
  if (!nh_->getParam(name, variable)) {        \
    ROS_FATAL("MISSING PARAMETER: %s.", name); \
    ros::shutdown();                           \
  }

// Make a spherical marker for visualization in rviz
// Colors are 0:red, 1:green, 2:blue
visualization_msgs::Marker makeSphereMarker(
    double x, double y, double z, int color = 1,
    std::string base_frame = "robot_map") {
  static int id = 0;
  visualization_msgs::Marker marker;
  marker.header.frame_id = base_frame;
  marker.header.stamp = ros::Time();
  marker.id = id++;
  marker.type = visualization_msgs::Marker::SPHERE;
  marker.action = visualization_msgs::Marker::ADD;
  marker.pose.position.x = x;
  marker.pose.position.y = y;
  marker.pose.position.z = z;
  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = 0.0;
  marker.pose.orientation.w = 0.0;
  double radius = 0.06;
  marker.scale.x = marker.scale.y = marker.scale.z = radius;

  std::vector<float> colors(3);
  color = color % 3;
  colors[color] = 1.0f;
  marker.color.r = colors[0];
  marker.color.g = colors[1];
  marker.color.b = colors[2];
  marker.color.a = 1.0;
  marker.lifetime = ros::Duration(0.1);

  return marker;
}

visualization_msgs::Marker makeSphereMarker(
    sva::PTransformd& p, int color = 1, std::string base_frame = "robot_map") {
  return makeSphereMarker(p.translation()[0], p.translation()[1],
                          p.translation()[2], color, base_frame);
}

visualization_msgs::Marker makeSphereMarker(
    Eigen::Vector3d& p, int color = 1, std::string base_frame = "robot_map") {
  return makeSphereMarker(p[0], p[1], p[2], color, base_frame);
}

#endif
