#include <iostream>
#include <vector>

/**
* @brief Binary search - returns the index of the element within the sorted array, or -1 if not found
*/
template <typename T>
int binary_search(const std::vector<T>& sorted, const T& element)
{
  if (sorted.empty())
    return -1;

  int left = 0;
  int right = sorted.size() - 1;

  int mid = sorted.size();

  while (right > left)
  {
    mid = left + (right - left) / 2;  // implicit floor

    // printf("\nleft, right, mid, sorted[mid], element: %d, %d, %d, %d, %d", left, right, mid, sorted[mid], element);

    if (sorted[mid] < element)
      left = mid + 1;
    else
      right = mid;
  }

  return sorted[left] == element ? left : -1;
}

template <typename T>
int run_binary_search(const std::vector<T>& sorted, const T& element)
{
  int idx = binary_search(sorted, element);
  if (idx != -1)
    std::cout << element << " is in index: " << idx << ". sanity check: " << sorted[idx] << std::endl;
  else
    std::cout << element << " is not in list" << std::endl;
}

int main()
{
  const std::vector<int> test_array = {1, 2, 3, 4, 5, 10, 22, 40, 100};

  static constexpr size_t N = 100000;
  std::vector<int> test_array_2(N);
  for (int i = 0; i < N; i++)
    test_array_2[i] = i;

  run_binary_search(std::vector<int>(), 2);
  run_binary_search(std::vector<int>({1}), 5);
  run_binary_search(std::vector<int>({5}), 5);
  run_binary_search(test_array, 2);
  run_binary_search(test_array, 40);
  run_binary_search(test_array, 1);
  run_binary_search(test_array, 100);
  run_binary_search(test_array, -10);
  run_binary_search(test_array, 150);

  /// \TODO{} compare against std::

  return 0;
}