/*

Code for Problems 1.6.3, 1.6.4

5. If 1MB of free storage was a hard limit - split bitmap method into two parts.
6. If each number could appear at most ten times: instead of vector<bool> (aka bitmap), use vector<uint4> 2^4=12 so 4
bits is enough to represent how many times each number appears.

*/

#include <bitset>
#include <vector>
#include <unordered_set>
#include <iostream>
#include <cassert>
#include <chrono>
#include <algorithm>

typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::milliseconds ms;
typedef std::chrono::duration<float> fsec;

static constexpr int num_random_numbers = 10e6;
static constexpr int max_int = 10e7;

/// num: number of random numbers
/// max: maximum random number. range is [0, max]
std::vector<unsigned int> generateRandomIntegers(unsigned int num, unsigned int max)
{
  assert(max >= num);
  std::unordered_set<unsigned int> random_numbers;
  unsigned int rand_num;

  /// \TODO{} don't use unordered_set - find a more efficient way of checking for duplicates. consider memory + speed
  ///         * map? that's pretty much what set does though

  // ignore random numbers above RAND_MAX - (RAND_MAX % max)) to give all number equal probability to all within range
  // e.g. if RAND_MAX=12, max=4, then 10%5=0 so we want to ignore >= 10. 12 - (12 % 5) = 10
  unsigned int ignore_greater_than_or_equal = RAND_MAX - (RAND_MAX % (max + 1));

  while (random_numbers.size() < num)
  {
    rand_num = rand();
    if (rand_num >= ignore_greater_than_or_equal)
      continue;
    rand_num = rand_num % (max + 1);
    random_numbers.insert(rand_num);
  }

  std::vector<unsigned int> output(random_numbers.size());
  std::copy(random_numbers.begin(), random_numbers.end(), output.begin());

  return output;
}

std::vector<unsigned int> sortBitmap(const std::vector<unsigned int>& numbers)
{
  // initialize all to zero
  std::vector<bool> bitmap(max_int, 0);

  // fill bitmap
  for (const auto& num : numbers)
    bitmap[num] = 1;

  // fill sorted vector
  std::vector<unsigned int> sorted;
  sorted.reserve(numbers.size());
  for (int i = 0; i < max_int; ++i)
    if (bitmap[i])
      sorted.push_back(i);

  return sorted;
}

void sortStd(std::vector<unsigned int>& numbers)
{
  std::sort(numbers.begin(), numbers.end());
}

int main()
{
  auto t0 = Time::now();
  auto random_numbers = generateRandomIntegers(num_random_numbers, max_int);
  auto t1 = Time::now();
  std::cout << "Time to generate random numbers  - " << std::chrono::duration_cast<ms>(t1 - t0).count() << std::endl;

  t0 = Time::now();
  sortStd(random_numbers);
  t1 = Time::now();
  std::cout << "Time to sort with std::sort - " << std::chrono::duration_cast<ms>(t1 - t0).count() << std::endl;

  t0 = Time::now();
  sortBitmap(random_numbers);
  t1 = Time::now();
  std::cout << "Time to sort with bitmap - " << std::chrono::duration_cast<ms>(t1 - t0).count() << std::endl;

  return 0;
}