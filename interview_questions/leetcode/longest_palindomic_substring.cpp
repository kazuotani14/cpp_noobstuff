#include <algorithm>
#include <iostream>
#include <string>
#include <stack>
#include <vector>

using namespace std;

bool is_palindrome(string s) {
   string s2 = s;
   reverse(s2.begin(), s2.end());
   // if(s2==s) cout << s << " is a palindrome" << endl;
   // else cout << s << " is not a palindrome" << endl;
   return (s2==s);
}

// O(n^2) solution: just check for longest from each letter
// Too slow
// Most practical (simple) algorithms seem to be O(n^2), although some are faster than this one in practice
// string longestPalindrome(string s) {
//    int max_pal_len = 1;
//    int start = 0;
//    for(int i=0; i<s.size(); ++i) {
//        for(int len=2; len<=(s.size()-i); ++len) {
//           if(is_palindrome(s.substr(i, len))) {
//              if(len > max_pal_len) {
//                 max_pal_len = len;
//                 start = i;
//              }
//           }
//        }
//     }
//     return s.substr(start, max_pal_len);
// }

template <typename T>
void print_matrix(vector<vector<T>> m) {
  for(auto row : m) {
    for(auto elem : row) {
      cout << elem << " ";
    }
    cout << "\n";
  }
}

// Dynamic programming: a string between indices (i,j) is a palindrome if (i+1, j-1) is a palindrome and S(i)==S(j). This has more space complexity: O(n^2), but it's cheaper computationally.
// Base cases: P(i,i)=true, P(i,i+1)=(S[i]==S[i+1]). Work up from there
string longestPalindrome(string s) {
  int start = 0;
  int max_sub_len = 1;

  // Initialize palindrome table
  vector<vector<bool>> P; // true if S(i,j) is palindrome. only need to do upper triangle
  P.resize(s.size());
  for(int i=0; i<s.size(); ++i) {
    P[i].resize(s.size());
    P[i][i] = true;
    if(i < s.size()-1 && (s[i]==s[i+1])){
      P[i][i+1] = true;
      start = i;
      max_sub_len = 2;
    }
  }

  // go thru other substrings, starting from shorter ones
  for(int len=3; len<s.size()+1; ++len) {
    for(int i=0; i<s.size(); ++i) {
      int j = i+len-1; // substring from i to j
      if(j >= s.size()) continue;
      if(P[i+1][j-1] && s[i]==s[j]) {
        P[i][j] = true;
        if(len > max_sub_len) {
          start = i;
          max_sub_len = len;
        }
      }
    }
  }

  return s.substr(start, max_sub_len);
}

int main(int argc, char* argv[]) {
  auto test = [](string s){
    string s2 = longestPalindrome(s);
    cout << s << " - " << s2 << endl;
  };
  test("a");
  test("abcba");
  test("cbbd");

  return 0;
}
