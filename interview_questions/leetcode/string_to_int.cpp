#include <algorithm>
#include <exception>
#include <iostream>
#include <limits>
#include <stack>
#include <string>
#include <vector>

using namespace std;

/*
Implement atoi to convert a string to an integer.

Hint: Carefully consider all possible input cases. If you want a challenge, please do not see below and ask yourself what are the possible input cases.

Notes: It is intended for this problem to be specified vaguely (ie, no given input specs). You are responsible to gather all the input requirements up front.

Whitespace, negatives, INT_MAX (2147483647) or INT_MIN (-2147483648)

This turned out to be a poorly defined problem; didn't finish it
*/

int myAtoi(string str) {
  int result = 0;
  int sign = 1;
  int char_count = 0;
  for(int i=0; i<str.size(); ++i) {
    char c = str[i];
    int num = c - '0';
    cout << c << endl;

    if(char_count==0 && c=='-') {
      sign = -1;
      char_count++;
      continue;
    }
    else if(num<0 || num>9) { // all other special characters
        if(char_count==0) {
          char_count++;
          continue;
        }
        else return result;
    }

    if((INT_MAX-num)/10 < result) return 0;
    if((INT_MIN+num)/10 > result) return 0;

    result = result*10 + sign*num;
  }
  return result;
}

int main(int argc, char* argv[]) {
  // cout << myAtoi("1") << endl;
  // cout << myAtoi("12") << endl;
  // cout << myAtoi("-15") << endl;
  // cout << myAtoi("     010") << endl;
  // cout << myAtoi("+-2") << endl;
  cout << myAtoi("  -0012a42") << endl;


  // cout << myAtoi("2147483648") << endl;
  // cout << myAtoi("-2147483649") << endl;
  return 0;
}
