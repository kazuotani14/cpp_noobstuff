#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <stack>
#include <string>
#include <vector>

using namespace std;

void print_vec(vector<int>& v) {
    for(auto n : v) cout << n << " ";
    cout << "\n";
}

void QuickSort(vector<int>& nums, int l, int r) {
    if(l >= r) return;
    int pivot_num = nums[r];
    int last_less = l;
    for(int i=l; i<r; ++i) {
        if(nums[i] < pivot_num) {
            swap(nums[i], nums[last_less++]);
        }
    }
    swap(nums[r], nums[last_less]);

    QuickSort(nums, l, last_less-1);
    QuickSort(nums, last_less+1, r);
}

int firstMissingPositive(vector<int>& nums) {
    QuickSort(nums, 0, nums.size()-1);

    int missing = 1;
    for(auto n : nums) {
        if (n==missing) missing++;
    }

    return missing;
}

int main(int argc, char* argv[]) {
  vector<int> nums = {6, 4, 0, 2, 1};
  cout << firstMissingPositive(nums) << endl;

  return 0;
}
