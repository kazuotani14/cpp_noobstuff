#include <algorithm>
#include <iostream>
#include <stack>
#include <string>
#include <vector>

using namespace std;

int longestValidParentheses(string s) {
    if(s.size()==0 || s.size()==1) return 0;
    stack<int> opens;
    int longest = 0;

    opens.push(-1); // top of opens is last index before valid substring
    for(int i=0; i<s.size(); ++i) {
        if(s[i] == ')') {
            if(!opens.empty()) {
                opens.pop();
                if(opens.empty()){
                    opens.push(i);
                }
                else {
                    int cur_len = i - opens.top();
                    longest = max(longest, cur_len);
                }
            }
        }
        else if (s[i] == '(') {
            opens.push(i);
        }
        else {
            cout << "invalid string " << endl;
            return 0;
        }
    }
    return longest;
}

int main(int argc, char* argv[]) {
  cout << longestValidParentheses("()()") << endl; // expect 4
  cout << longestValidParentheses("(()") << endl;  // expect 2
  cout << longestValidParentheses("()(())") << endl; // expect 6
  cout << longestValidParentheses(")(") << endl;
  cout << longestValidParentheses("(") << endl;
  return 0;
}
