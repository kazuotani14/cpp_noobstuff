/*
Given a linked list, reverse the nodes of a linked list k at a time and return its modified list.

k is a positive integer and is less than or equal to the length of the linked list. If the number of nodes is not a multiple of k then left-out nodes in the end should remain as it is.

You may not alter the values in the nodes, only nodes itself may be changed.

Only constant memory is allowed.

For example,
Given this linked list: 1->2->3->4->5

For k = 2, you should return: 2->1->4->3->5

For k = 3, you should return: 3->2->1->4->5
*/

#include <iostream>
#include <vector>

using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};


void print_list(ListNode* head) {
    if(head==NULL) return;
    int count = 0;
    cout << "list: ";
    ListNode* node = head;
    while(node != NULL) {
        cout << node->val << " ";
        node = node->next;
        if(count++ > 50) break;
    }
    cout << "\n";
}

ListNode* reverseKGroup(ListNode *head, int k) {
    if(head==NULL || k==1) return head;

    int num=0;
    ListNode* preheader = new ListNode(-1);
    preheader->next = head;
    ListNode* cur = preheader;
    ListNode* pre = preheader;
    ListNode* nex;

    // Find number of elements + 1
    while(cur) {
        cur = cur->next;
        num++;
    }

    // Notice: num starts at top, but index moves from start
    while(num >= k) {
        cur = pre->next;
        nex = cur->next;
        // cur: start of current section. cur->next moved backwards each iteration
        // nex: pushed backwards each iteration
        // pre: node before current section, pre->next pointed to nex each iteration
        for(int i=0; i<k-1; ++i) {
            cur->next = nex->next; // move pointer from front of reversed section (new back)
            nex->next = pre->next; // point new_node->next backwards
            pre->next = nex; // pre points to new node
            nex = cur->next; // new node moved forward
        }
        pre = cur;
        num -= k;
    }
    return preheader->next;
}

int main() {
    vector<int> input = {1, 2, 3, 4, 5, 6}; int k = 4;
    // vector<int> input = {}; int k = 1;
    // vector<int> input = {1}; int k = 1;


    //make linked list
    ListNode* node = input.empty() ? NULL : new ListNode(input[0]);
    ListNode* head = node;
    for(int i=1; i<input.size(); ++i) {
        node->next = new ListNode(input[i]);
        node = node->next;
    }

    ListNode* new_head = reverseKGroup(head, k);
    print_list(new_head);
}
