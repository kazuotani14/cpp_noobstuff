/*
You are given a string, s, and a list of words, words, that are all of the same length. Find all starting indices of substring(s) in s that is a concatenation of each word in words exactly once and without any intervening characters.

For example, given:
s: "barfoothefoobarman"
words: ["foo", "bar"]

You should return the indices: [0,9].
(order does not matter).
*/

#include <algorithm>
#include <iostream>
#include <stack>
#include <string>
#include <vector>

// Unordered_map always faster than map for single-element access, but takes more memory
// unordered_map is hash table, (ordered) map is tree 
#include <unordered_map>

using namespace std;

// vec.erase(std::remove(vec.begin(), vec.end(), int_to_remove), vec.end());

// template <typename T>
// void print_vec(vector<T> v) {
//   for(auto e : v) cout << e << "  ";
//   cout << "\n";
// };

int factorial(int n) {
  return (n>0) ? n * factorial(n-1) : 1;
}

// Swap the rightmost element with all the other elements, and then recursively call the permutation function on the subset on the left.
void generatePermutations(int n, vector<string> words, unordered_map<string, bool>& concats){
  if (n==0) {
    string perm = "";
    for(auto w : words) perm += w;
    concats[perm] = true;
  }
  else {
    for(int i=n-1; i>=0; --i) {
        swap(words[i],words[n-1]); // swap rightmost with i
        generatePermutations(n-1, words, concats);
        swap(words[i],words[n-1]); // swap back to original order for next iteration
    }
  }
}

// Brute force method - works but too slow
// Scales with n! for number of words
vector<int> findSubstring(string s, vector<string>& words) {
  vector<int> start_idx;
  if(s.size()==0 || words.empty()) return start_idx;

  // 1. Make all possible combinations of concatenated substrings
  unordered_map<string, bool> concats;
  generatePermutations(words.size(), words, concats);

  // 2. Search all substrings of length(concatenated)
  int concat_len = 0;
  for(auto& w : words) concat_len += w.size();
  if(concat_len > s.size()) return start_idx;

  for(int start=0; start<=(s.size()-concat_len); ++start) {
    string sub = s.substr(start, concat_len);
    if(concats.find(sub) != concats.end()) {
      if(find(start_idx.begin(), start_idx.end(), start) == start_idx.end())
        start_idx.push_back(start);
    }
  }
  return start_idx;
}

//  map of words -> occurences. Go through each possible substring of length(concat)
vector<int> findSubstring_better(string s, vector<string>& words) {
  unordered_map<string, int> expected_count;
  for(const auto& w : words) expected_count[w]++;

  int num_words = words.size();
  int w_len = words[0].size();
  int c_len = num_words*w_len;
  if(s.size() < c_len) return {};

  vector<int> start_idx;
  for(int start=0; start<=(s.size()-num_words*w_len); ++start) {
    unordered_map<string, int> seen;

    string sub = s.substr(start, c_len);
    int j = 0;
    for(; j<num_words; ++j) {
      string w_cand = s.substr(start+w_len*j, w_len);
      if(expected_count.find(w_cand) != expected_count.end()) {
        seen[w_cand]++;
        if(seen[w_cand] > expected_count[w_cand]) break;
      }
      else {
        break;
      }
    }
    if(j == num_words) start_idx.push_back(start);
  }
  return start_idx;
}


int main(int argc, char* argv[]) {

  // Generic lambda
  auto print_vec = [](auto v) {
    for(auto e : v) cout << e << "  ";
    cout << "\n";
  };

  string s;
  vector<string> words;

  // vector<string> concats;
  // words = {"1", "2", "3"};
  // generatePermutations(words.size(), words, concats);
  // print_vec(concats);

  // s = "barfoothefoobarman"; words = {"foo", "bar"};
  // s = "a"; words = {"a", "a"};
  s = "barfoofoobarthefoobarman"; words = {"bar","foo","the"};

  vector<int> start_indices = findSubstring(s, words);
  print_vec(start_indices);

  return 0;
}
