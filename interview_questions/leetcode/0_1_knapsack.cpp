/*
  0/1 knapsack problem

  Dynamic programming: start by considering just a set of one item, and see what
  the best score that can be achieved for each weight is. Then, for every
  iteration after that, add one item into consider and do: (for n items and w
  weight) V[n][w] = max(V(don't take item n), V(take item n)) = max(V[n-1][w],
  V[n-1][w - w_n]) This should give maximum value at V[n_items][max_weight]

  Note: there is a column and row for zero!

  To reconstruct optimal set: move backwards from maximum value in table,
  inferring if item was included or not using dynamic programming equation

  Remember: {1, 2, 3, 4, 4, 5, 5, 6} as a way to make sample problem
  (alternating w, v insertions) Expect: with weight 7, value 9 Note: a knapsack
  problem without 0/1 (items can be split) can be solved with a greedy approach
*/

#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

template <typename T>
using Matrix = vector<vector<T>>;

int main(int argc, char* argv[]) {
  int capacity = 7;
  vector<int> weights{1, 3, 4, 5};
  vector<int> values{2, 4, 5, 6};

  // make a table with first row and first column zero
  Matrix<int> dp_table(weights.size() + 1);
  for (auto& row : dp_table) {
    row.resize(capacity + 1);
    row[0] = 0;
  }
  for (auto& elem : dp_table[0]) elem = 0;

  // Solve for max value
  // row = n items, col = w
  int w_item, v_item;
  for (int n = 1; n < dp_table.size(); ++n) {
    for (int w = 1; w < (capacity + 1); ++w) {
      w_item = weights[n - 1];
      v_item = values[n - 1];
      if (w_item <= w) {
        dp_table[n][w] =
            max(dp_table[n - 1][w], v_item + dp_table[n - 1][w - w_item]);
      } else {
        dp_table[n][w] = dp_table[n - 1][w];
        continue;
      }
    }
  }

  cout << "DP table: " << endl;
  for (auto& row : dp_table) {
    for (auto& elem : row) {
      cout << elem << " ";
    }
    cout << "\n";
  }

  cout << "max value: " << dp_table[dp_table.size() - 1][dp_table[0].size() - 1]
       << endl;

  // Find optimal set: work backwards from bottom right
  int i = dp_table.size() - 1;
  int j = dp_table[0].size() - 1;
  int cell_val;
  vector<int> optimal_set;
  while (true) {
    if (i == 0 || j == 0) break;
    cell_val = dp_table[i][j];
    v_item = values[i - 1];
    w_item = weights[i - 1];
    std::cout << i << ", " << j << " " << cell_val << std::endl;

    // this value came from one of two places
    if (cell_val == values[i - 1] + dp_table[i - 1][j - w_item]) {
      optimal_set.push_back(i - 1);
      i = i - 1;
      j = j - w_item;
    } else if (cell_val == dp_table[i - 1][j]) {
      i = i - 1;
    } else {
      cout << "Something went wrong!" << endl;
    }
  }

  cout << "optimal item set indices: ";
  for (auto& e : optimal_set) cout << e << " ";

  return 0;
}
