/*
  Solve a sudoku board given as a matrix of chars
  Empty elements are given as '.'

*/

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <stack>
#include <string>
#include <vector>
#include <utility>

using namespace std;

template <typename T>
using Matrix = vector<vector<T>>;

template <typename T>
void print_vec(vector<T>& v) {
    for(auto n : v) cout << n << " ";
    cout << "\n";
}

template <typename T>
void print_board(Matrix<T>& board) {
  for(auto& row : board) {
    for(auto& num : row) cout << num << " ";
    cout << "\n";
  }
}

class Solution {

public:

void removeNumberFromList(vector<int>& list, int num) {
  list.erase(std::remove(list.begin(), list.end(), num), list.end());
}

bool isSolved(vector<vector<char>>& board) {
  int board_size_ = board.size();
  for(int i=0; i<board_size_; ++i) {
    for(int j=0; j<board_size_; ++j) {
      if(board[i][j] == '.') return false;
    }
  }
  return true;
}

// Removes val in (i,j) from list of possible values in row, column, cell
void propagateConstraint(int i, int j, int val, vector<vector<vector<int>>>& possible_values) {
  int cell_size = sqrt(board_size_);

  // propagate this value across row and column
  for(int col=0; col<board_size_; ++col) removeNumberFromList(possible_values[i][col], val);
  for(int row=0; row<board_size_; ++row) removeNumberFromList(possible_values[row][j], val);

  //propagate this value to all values in cell
  int cell_start_row = cell_size * (i/cell_size);
  int cell_start_col = cell_size * (j/cell_size);
  for(int row=0; row<cell_size; ++row) {
    for(int col=0; col<cell_size; ++col) {
      removeNumberFromList(possible_values[cell_start_row+row][cell_start_col+col], val);
    }
  }
}

void initBoard(vector<vector<char>>& board) {
  vector<int> all_nums;
  for(int i=1; i<=board_size_; ++i) all_nums.push_back(i);

  possible_values_.resize(board_size_);
  for(int i=0; i<board_size_; ++i) {
    possible_values_[i].resize(board_size_);
    for(int j=0; j<board_size_; ++j) {
        possible_values_[i][j] = all_nums;
      }
    }
  }

  for(int i=0; i<board_size_; ++i) {
    for(int j=0; j<board_size_; ++j) {
      if(board[i][j] == '.') continue;
      int val = board[i][j] - '0';
      propagateConstraint(i, j, val, possible_values_);
    }
  }
}

void solveDetermined(vector<vector<char>>& board, vector<vector<vector<int>>>& possible_values) {
  int board_size_ = board.size();
  bool new_fills = true;
  while(new_fills) {
    new_fills = false;
    for(int i=0; i<board_size_; ++i) {
      for(int j=0; j<board_size_; ++j) {
        if(possible_values[i][j].size() != 1) continue;
        new_fills = true;
        board[i][j] = '0' + possible_values[i][j][0];
        propagateConstraint(i, j, possible_values[i][j][0], possible_values);
      }
    }
  }
}

// Naive DFS
// Better method: backtracking with pruning of possibilities at each guess
bool backtrackSolve(vector<vector<char>>& board, int pos) {
  if (pos >= (board_size_*board_size_)) return true;
  int i = pos / board_size_;
  int j = pos % board_size_;
  if (board[i][j] != '.') {
    return backtrackSolve(board, pos + 1);
  }
  else {
    for (char c = '1'; c <= '0'+board_size_; c++) {
      if (!isInRow(board, i,c) && !isInCol(board, j, c) && !isInRec(board, i, j, c)) {
        board[i][j] = c;
        if (backtrackSolve(board, pos + 1))
          return true;
        else
          board[i][j] = '.';
      }
    }
    return false;
  }
}

bool isInRow(vector<vector<char>>& board, int i, char c)
{
  for (int k = 0; k < board_size_; k++) {
    if (board[i][k] == c) return true;
  }
  return false;
}

bool isInCol(vector<vector<char>>& board,int j, char c) {
  for (int k = 0; k < board_size_; k++) {
    if (board[k][j] == c) return true;
  }
  return false;
}

bool isInRec(vector<vector<char>>& board, int i, int j, char c) {
  int bigrow = i/cell_size_, bigcol = j/cell_size_;
  for (int m = cell_size_*bigrow; m < cell_size_*(bigrow + 1); m++) {
    for (int n = cell_size_*bigcol; n < cell_size_*(bigcol + 1); n++)
      if (board[m][n] == c)
        return true;
  }
  return false;
}

// For each cell that has only one possibility, fill and propagate possible values
void solveSudoku(vector<vector<char>>& board) {
  board_size_ = board.size();
  cell_size_ = sqrt(board_size_);

  // 1. Make table that stores all possible values for each cell
  initBoard(board);

  // 2. Go thru possible values and fill in ones that are fully determined
  solveDetermined(board, possible_values_);
  if(isSolved(board)) return;

  // 3. Guess for cell with least ambiguity, keep list of guesses to backtrack (brute force)
  // Basically DFS thru tree of possibilities
  backtrackSolve(board, 0);
}

private:
  int board_size_;
  int cell_size_;
  Matrix<vector<int>> possible_values_;

}; // end Solution

int main(int argc, char* argv[]) {
  Solution sol;
  vector<vector<char>> board_4x4 = { {'3', '4', '.', '2'},
                                 {'.', '.', '4', '3'},
                                 {'.', '.', '3', '4'},
                                 {'.', '3', '.', '1'}
                               };

  vector<vector<char>> board_9x9 = {
    {'.', '.', '9', '7', '4', '8', '.', '.', '.'},
    {'7', '.', '.', '.', '.', '.', '.', '.', '.'},
    {'.', '2', '.', '1', '.', '9', '.', '.', '.'},
    {'.', '.', '7', '.', '.', '.', '2', '4', '.'},
    {'.', '6', '4', '.', '1', '.', '5', '9', '.'},
    {'.', '9', '8', '.', '.', '.', '3', '.', '.'},
    {'.', '.', '.', '8', '.', '3', '.', '2', '.'},
    {'.', '.', '.', '.', '.', '.', '.', '.', '6'},
    {'.', '.', '.', '2', '7', '5', '9', '.', '.'}
  };

  sol.solveSudoku(board_4x4);
  print_board(board_4x4);

  sol.solveSudoku(board_9x9);
  print_board(board_9x9);

  return 0;
}
