/*
  https://leetcode.com/problems/zigzag-conversion/description/

  1. Define problem first - what does zigzag mean? Sample/test cases
*/

#include <algorithm>
#include <iostream>
#include <stack>
#include <string>
#include <vector>

using namespace std;

string convert(string s, int numRows) {
  if (numRows==1) return s;

  vector<vector<char>> zigzag(numRows);
  int line = 0;
  int step = 1;
  for(auto c : s) {
    zigzag[line].push_back(c);

    // Basically, just bounce line number back and forth between limits
    if(line == numRows-1) step = -1;
    else if (line == 0) step = 1;
    line += step;
  }

  string result;
  for(auto row : zigzag) {
      for(auto c : row) {
          result += c;
      }
  }
  return result;
}

int main(int argc, char* argv[]) {
  // cout << "hello!" << endl;
  cout << convert("PAYPALISHIRING", 3) << endl;
  cout << convert("ABCD", 2) << endl;
  cout << convert("ABC", 1) << endl;

  return 0;
}
