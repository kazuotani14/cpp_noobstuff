#include <algorithm>
#include <iostream>
#include <stack>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

using namespace std;

/*
Return edge to be removed from directed graph that would result in tree structure
In tree: each node only has one parent, except root (zero parents)

1) Check whether there is a node having two parents.
    If so, store them as candidates A and B, and set the second edge invalid.
2) Perform normal union find.
    If the tree is now valid
           simply return candidate B
    else if candidates not existing
           we find a circle, return current edge;
    else
           remove candidate A instead of B.

*/

// Recursively find root of set
int root(int node, vector<int>& node_sets) {
  if(node_sets[node] != node) {
    return root(node_sets[node], node_sets);
  }
  return node_sets[node];
}

vector<int> findRedundantDirectedConnection(vector<vector<int>>& edges) {
  // check for node that has two parents
  int n = edges.size();
  vector<int> parents(n+1, 0); // node are numbered 1~N, assume no skipping
  vector<int> candA, candB;
  for(auto& e : edges) {
    if(parents[e[1]] == 0){
      parents[e[1]] = e[0];
    }
    else {
      candA = {parents[e[1]], e[1]};
      candB = e;
      e[1] = 0; // set second edge invalid (before checking for other problems)
    }
  }

  // disjoint sets
  vector<int> node_sets(n+1);
  for(int i=0; i<node_sets.size(); ++i) node_sets[i] = i; // each node starts as its own parent

  for(auto& e : edges) {
    if(e[1]==0) continue;
    int u = e[0], v = e[1], pu = root(u, node_sets);
    if(v == pu) { // detected a circle (not necesarily a cycle)
      if(candA.empty()) return e;
      else return candA;
    }
    node_sets[v] = u; // (union)
  }
  return candB;
}

int main(int argc, char* argv[]) {
  vector<vector<int>> edges = {{4,2}, {1,5}, {5,2}, {5,3}, {2,4}};
  vector<int> r_edge = findRedundantDirectedConnection(edges);
  cout << "{ " << r_edge[0] << ", " << r_edge[1] << " }" << endl;

  return 0;
}
