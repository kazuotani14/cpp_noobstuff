#A Dynamic Programming based Python Program for 0 - 1 Knapsack problem
#Returns the maximum value that can be put in a knapsack of capacity W
def knapSack(W, wt, val, n):
#K contains max value for each(w, i) combination
#At each row i : best value considering items 0, ..., i
	K = [[0 for x in range(W+1)] for x in range(n+1)]

#Build table K[][] in bottom up manner
	for i in range(n+1):
		for w in range(W+1):
			if i==0 or w==0:
				K[i][w] = 0
			elif wt[i-1] <= w: # if this item can fit in this weight
#val[i - 1] + K[i - 1][w - wt[i - 1]] : take this item
#K[i - 1][w] : don't take this item
				K[i][w] = max(val[i-1] + K[i-1][w-wt[i-1]], K[i-1][w])
			else:
				K[i][w] = K[i-1][w] # can't take this item, so just use best value from weight under this
                        for k in K:
                                print(k)
                        print("---")
	return K[n][W]

#Driver program to test above function
val = [5, 15, 10]
wt = [1, 2, 3]
W = 5
n = len(val)
print(knapSack(W, wt, val, n))
